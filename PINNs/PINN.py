import torch.nn as nn
from .activations import activation


class PINN(nn.Module):
    """
    Implements the physics-informed neural network.
    """

    def __init__(self, input_dim, output_dim, num_hidden_layers, num_neurons, activation_f):
        super(PINN, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.n_hidden_layers = num_hidden_layers
        self.neurons = num_neurons
        self.activation = activation(activation_f)

        self.input_layer = nn.Linear(self.input_dim, self.neurons)
        self.hidden_layers = nn.ModuleList(
            [nn.Linear(self.neurons, self.neurons) for _ in range(self.n_hidden_layers - 1)])
        self.output_layer = nn.Linear(self.neurons, self.output_dim)

    def forward(self, x):
        x = self.activation(self.input_layer(x))
        for _, l in enumerate(self.hidden_layers):
            x = self.activation(l(x))
        return self.output_layer(x)


def init_xavier(model):
    """
    Glorot uniform initialization.
    """
    def init_weights(m):
        if type(m) == nn.Linear and m.weight.requires_grad and m.bias.requires_grad:
            # gain = nn.init.calculate_gain('tanh')
            gain = 1
            nn.init.xavier_uniform_(m.weight, gain=gain)
            nn.init.uniform_(m.bias, 0, 1)

            # nn.init.xavier_uniform_(m.bias)
            # m.bias.data.fill_(0)

    model.apply(init_weights)
