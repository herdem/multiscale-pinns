import torch.nn as nn
from .activations import activation


class PINN_resnet(nn.Module):
    """
    Implements the physics-informed neural network.
    """

    def __init__(self, input_dim, output_dim, num_hidden_layers, num_neurons, activation_f):
        super(PINN_resnet, self).__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.n_hidden_layers = num_hidden_layers
        self.neurons = num_neurons
        self.activation = activation(activation_f)

        self.input_layer = nn.Linear(self.input_dim, self.neurons)
        n_res_blocks = (num_hidden_layers - 1) // 2
        n_remaining_layers = (num_hidden_layers - 1) % 2
        self.residual_blocks = nn.ModuleList(
            [ResidualBlock(self.neurons, activation_f) for _ in range(n_res_blocks)])
        if n_remaining_layers == 0:
            self.remaining_layers = None
        else:
            self.remaining_layers = nn.ModuleList(
                [nn.Linear(self.neurons, self.neurons) for _ in range(n_remaining_layers)])

        self.output_layer = nn.Linear(self.neurons, self.output_dim)

    def forward(self, x):
        x = self.activation(self.input_layer(x))
        for block in self.residual_blocks:
            x = block(x)
        for layer in self.remaining_layers:
            x = self.activation(layer(x))
        return self.output_layer(x)


class ResidualBlock(nn.Module):
    """
    Defines a residual block consisting of two layers.
    """

    def __init__(self, num_neurons, activation_f):
        super(ResidualBlock, self).__init__()
        self.neurons = num_neurons
        self.activation = activation(activation_f)

        self.layer_1 = nn.Linear(self.neurons, self.neurons)
        self.layer_2 = nn.Linear(self.neurons, self.neurons)

    def forward(self, x):
        z = self.activation(self.layer_1(x))
        z = self.layer_2(z)
        z = z + x
        return self.activation(z)


def init_xavier(model):
    """
    Glorot uniform initialization.
    """
    def init_weights(m):
        if type(m) == nn.Linear and m.weight.requires_grad and m.bias.requires_grad:
            # gain = nn.init.calculate_gain('tanh')
            gain = 1
            nn.init.xavier_uniform_(m.weight, gain=gain)
            nn.init.uniform_(m.bias, 0, 1)

            # nn.init.xavier_uniform_(m.bias)
            # m.bias.data.fill_(0)

    model.apply(init_weights)
