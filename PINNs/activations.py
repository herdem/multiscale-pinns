import torch
import torch.nn as nn


class Swish(nn.Module):
    """
    The swish activation function: x * \sigma(x)
    """

    def __init__(self,):
        super(Swish, self).__init__()

    def forward(self, x):
        return x * torch.sigmoid(x)


class Sin(nn.Module):
    """
    The sin activation function: \sin(x)
    """

    def __init__(self,):
        super(Sin, self).__init__()

    def forward(self, x):
        return torch.sin(x)


class Snake(nn.Module):
    """
    The snake activation function: x + \sin(\alpha * x)^2 / \alpha
    """

    def __init__(self, alpha=0.5,):
        super(Snake, self).__init__()
        self.alpha = alpha

    def forward(self, x):
        return x + torch.sin(self.alpha * x) ** 2 / self.alpha


def activation(name):
    """
    Returns an activation function according to name.
    """
    if name in ['tanh', 'Tanh']:
        return nn.Tanh()
    elif name in ['relu', 'ReLU']:
        return nn.ReLU(inplace=True)
    elif name in ['lrelu', 'LReLU']:
        return nn.LeakyReLU(inplace=True)
    elif name in ['sigmoid', 'Sigmoid']:
        return nn.Sigmoid()
    elif name in ['softplus', 'Softplus']:
        return nn.Softplus(beta=4)
    elif name in ['celu', 'CeLU']:
        return nn.CELU()
    elif name in ['swish']:
        return Swish()
    elif name in ['sin']:
        return Sin()
    elif name in ['snake']:
        return Snake()
    else:
        raise ValueError('Unknown activation function')
