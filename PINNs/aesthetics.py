import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc
import numpy as np
import seaborn as sns


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)
# plt.rc('text.latex', preamble=r'\usepackage{euscript}')
SMALL_SIZE = 8
MEDIUM_SIZE = 14
BIGGER_SIZE = 16

plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the tick labels

plt.rc('axes', axisbelow=True)  # grid below other points

plt.rc('image', cmap='viridis')  # default colomap

plt.rc('axes', prop_cycle=plt.cycler(
    color=plt.cm.viridis.colors[::115] + plt.cm.viridis.colors[90::115]))  # default color cycler
sns.color_palette('viridis', as_cmap=True)

plt.rc('figure', dpi=500)
