import torch
from torch.utils.data import DataLoader, TensorDataset


class Dataset:
    """
    Defines the Dataset class for use with PINNs.
    """

    def __init__(self, equation, n_coll, n_b, n_init, n_internal, batches, random_seed, shuffle=False):
        self.n_coll = n_coll
        self.n_b = n_b
        self.n_init = n_init
        self.n_internal = n_internal
        self.batches = batches
        self.random_seed = random_seed
        self.shuffle = shuffle

        self.space_dimensions = equation.space_dimensions
        self.time_dimensions = equation.time_dimensions
        self.input_dimensions = equation.space_dimensions + equation.time_dimensions
        self.output_dimension = equation.output_dimension
        self.n_samples = self.n_coll + 2 * self.n_b * self.space_dimensions + \
            self.n_init * self.time_dimensions + self.n_internal
        self.BC = None
        self.data_coll = None

        if self.batches == 'full':
            self.batches = int(self.n_samples)
        else:
            self.batches = int(self.batches)

        fraction_coll = (self.batches * self.n_coll) // self.n_samples
        fraction_boundary = (self.batches * 2 * self.n_b *
                             self.space_dimensions) // self.n_samples
        fraction_initial = (self.batches * self.n_init) // self.n_samples
        fraction_internal = (self.batches * self.n_internal) // self.n_samples

        x_coll, y_coll = equation.add_collocation_points(
            self.n_coll, self.random_seed)
        x_b, y_b = equation.add_boundary_points(self.n_b, self.random_seed)
        x_time_internal, y_time_internal = equation.add_initial_points(
            self.n_init, self.random_seed)
        if self.n_internal != 0:
            x_internal, y_internal = equation.add_internal_points(
                self.n_internal, self.random_seed)
            x_time_internal = torch.cat([x_time_internal, x_internal], 0)
            y_time_internal = torch.cat([y_time_internal, y_internal], 0)

        if self.n_coll == 0:
            self.data_coll = DataLoader(TensorDataset(
                x_coll, y_coll), batch_size=1, shuffle=False)
        else:
            self.data_coll = DataLoader(TensorDataset(
                x_coll, y_coll), batch_size=fraction_coll, shuffle=self.shuffle)

        if self.n_b == 0:
            self.data_boundary = DataLoader(TensorDataset(
                x_b, y_b), batch_size=1, shuffle=False)
        else:
            self.data_boundary = DataLoader(TensorDataset(
                x_b, y_b), batch_size=fraction_boundary, shuffle=self.shuffle)

        if fraction_internal == 0 and fraction_initial == 0:
            self.data_initial_internal = DataLoader(TensorDataset(
                x_time_internal, y_time_internal), batch_size=1, shuffle=False)
        else:
            self.data_initial_internal = DataLoader(TensorDataset(
                x_time_internal, y_time_internal), batch_size=fraction_initial + fraction_internal, shuffle=self.shuffle)

    def get_data(self):
        """
        Returns the collocation data, boundary data and internal/initial data.
        """
        return self.data_coll, self.data_boundary, self.data_initial_internal
