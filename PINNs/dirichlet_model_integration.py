import torch.nn as nn
import torch


class Dirichlet_Wrapper(nn.Module):
    """
    Is thought to be an extension to the PINN.
    Transforms the input to conform to Dirichlet boundary conditions.

    TODO: assumes one space dim, all boundaries have constant Dirichlet boundary conditions,
    """

    def __init__(self, model, equation, rescale, G):
        super(Dirichlet_Wrapper, self).__init__()
        self.model = model
        self.output_dim = model.output_dim
        if equation.time_dimensions == 0:
            self.extrema = equation.extrema
            self.time = False
        else:
            self.extrema = equation.extrema[1:, :]
            self.time = True

        self.a = self.extrema[:, 0]
        self.b = self.extrema[:, 1]
        self.G = G
        self.rescale = rescale

    def forward(self, x):
        out = self.model(x)
        if self.time:
            x_ = x[:, 1:]
        else:
            x_ = x
        x = self.G(x, self.a, self.b) + torch.unsqueeze(torch.prod((x_ - self.a) *
                                                                   (x_ - self.b), dim=1), 1) * out * self.rescale
        return x
