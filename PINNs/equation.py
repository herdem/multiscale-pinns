import torch
import numpy as np
import colorsys
import matplotlib.colors as mc


class Equation:
    """
    Base class for the equations.
    """

    def __init__(self):
        self.device = torch.device(
            'cuda') if torch.cuda.is_available() else torch.device("cpu")

    def convert(self, vector, extrema):
        """
        Returns the vector scaled to the domain given by extrema.
        """
        vector = np.array(vector)
        max_val = np.max(np.array(extrema), axis=1)
        min_val = np.min(np.array(extrema), axis=1)
        vector = vector * (max_val - min_val) + min_val
        return torch.from_numpy(vector).type(torch.FloatTensor)

    def lighten_color(self, color, amount=0.5):
        try:
            c = mc.cnames[color]
        except:
            c = color
        c = colorsys.rgb_to_hls(*mc.to_rgb(c))
        return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

    def add_collocation_points(self, n_coll, random_seed):
        return torch.tensor([]), torch.tensor([])

    def add_initial_points(self, n_init, random_seed):
        return torch.tensor([]), torch.tensor([])

    def add_boundary_points(self, n_b, random_seed):
        return torch.tensor([]), torch.tensor([])

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        pass

    def apply_ic(self, model, x_u, u, u_pred_vars, u_train_vars):
        pass

    def compute_res(self, model, x_coll):
        raise NotImplementedError()
