import torch
import numpy as np
from tqdm import tqdm
from .loss import Loss


def fit(equation, model, optimizer, num_epochs, dataset, loss_weights, regularization_order=2, logger=None, log_frequency=25, alpha=5.):
    model.train()

    assert len(loss_weights) == 3
    w_r = loss_weights[0]
    w_vars = loss_weights[1]
    w_reg = loss_weights[2]

    losses = list([np.NAN, np.NAN, np.NAN, np.NAN, np.NAN])

    coll, boundary, initial_internal = dataset.get_data()

    t = tqdm(total=num_epochs)

    n = model.output_dim + 1
    w = torch.cat((w_r, torch.tensor([w_vars])))
    w.requires_grad = True
    weight_optimizer = torch.optim.Adam([w], lr=0.025)
    L_0 = torch.zeros(n)

    n_iter = 0
    dry_run = 1

    def closure():
        nonlocal n_iter, w, L_0
        optimizer.zero_grad()
        weight_optimizer.zero_grad()

        loss_r, loss_vars, loss_reg = Loss()(
            equation, model, x_coll, x_u, u, x_b, u_b, regularization_order)
        loss = torch.cat((loss_r, torch.unsqueeze(loss_vars, 0)), 0)
        weighted_loss = w * loss

        if n_iter < dry_run:
            L_0 += loss.data
        elif n_iter == dry_run:
            L_0 += loss.data
            L_0 = L_0.mean()

        loss = torch.sum(weighted_loss) + w_reg * loss_reg
        loss.backward(retain_graph=True)

        L_tilde = loss / L_0
        r = (L_tilde / L_tilde.mean()).detach()

        G = list()
        for idx in range(n):
            grad = torch.autograd.grad(weighted_loss[idx], model.hidden_layers[-1].weight, grad_outputs=torch.ones_like(
                weighted_loss[idx]), create_graph=True)[0]
            G.append(torch.linalg.norm(grad, ord=2))
        G = torch.stack(G)
        G_bar = G.mean().detach()

        if n_iter >= dry_run:
            L_grad = torch.linalg.norm(G - G_bar * (r ** alpha), ord=1)
            L_grad.backward()
            weight_optimizer.step()

        with torch.no_grad():
            unweighted_loss = torch.sum(loss_r) + loss_vars
            w.copy_(n * w / w.sum())

        if logger != None and n_iter % log_frequency == 0:
            logger.add_scalar('total_loss', loss, n_iter)
            for i in range(n):
                logger.add_scalar(f'loss_weight_{i + 1}', w[i], n_iter)
            for i in range(1, loss_r.shape[0] + 1):
                logger.add_scalar(f'residual_loss_{i}', loss_r[i - 1], n_iter)
            logger.add_scalar('vars_loss', loss_vars, n_iter)
            logger.add_scalar('regularization_loss', loss_reg, n_iter)
            for name, weight in model.named_parameters():
                logger.add_histogram(name, weight, n_iter)
                logger.add_histogram(f'{name}.grad', weight.grad, n_iter)
                logger.add_scalar(f'{name}.grad.avg',
                                  torch.mean(weight.grad), n_iter)
        t.set_postfix(total_loss=loss.detach().cpu().numpy().round(4), residual_loss=loss_r.detach(
        ).cpu().numpy().round(4), vars_loss=loss_vars.detach().cpu().numpy().round(4))
        losses[0] = loss
        losses[1] = unweighted_loss
        losses[2] = loss_r
        losses[3] = loss_vars
        losses[4] = loss_reg
        n_iter += 1
        return loss

    for epoch in range(num_epochs):
        t.set_description(f"Epoch {epoch + 1}/{num_epochs}")

        if len(boundary) != 0 and len(initial_internal) != 0:
            data = zip(coll, boundary, initial_internal)
            for _, ((x_coll, _), (x_b, u_b), (x_u, u)) in enumerate(data):
                x_coll = x_coll.to(equation.device)
                x_b = x_b.to(equation.device)
                u_b = u_b.to(equation.device)
                x_u = x_u.to(equation.device)
                u = u.to(equation.device)

                optimizer.step(closure=closure)
        elif len(boundary) == 0 and len(initial_internal) != 0:
            data = zip(coll, initial_internal)
            for _, ((x_coll, _), (x_u, u)) in enumerate(data):
                x_b = torch.full((0, x_u.shape[1]), 0)
                u_b = torch.full((0, x_u.shape[1]), 0)

                x_coll = x_coll.to(equation.device)
                x_b = x_b.to(equation.device)
                u_b = u_b.to(equation.device)
                x_u = x_u.to(equation.device)
                u = u.to(equation.device)

                optimizer.step(closure=closure)
        elif len(boundary) != 0 and len(initial_internal) == 0:
            data = zip(coll, boundary)
            for _, ((x_coll, _), (x_b, u_b)) in enumerate(data):
                x_u = torch.full((0, 1), 0)
                u = torch.full((0, 1), 0)

                x_coll = x_coll.to(equation.device)
                x_b = x_b.to(equation.device)
                u_b = u_b.to(equation.device)
                x_u = x_u.to(equation.device)
                u = u.to(equation.device)

                optimizer.step(closure=closure)

        t.update()

    t.close()
    return losses
