import torch
import numpy as np
import sobol_seq
from pyDOE import lhs


def generate_points(n, dim, random_seed, type_of_points, boundary):
    """
    Generate quadrature points.
    """

    # random points for Monte Carlo quadrature
    if type_of_points == "random":
        torch.random.manual_seed(random_seed)
        return torch.rand([n, dim]).type(torch.FloatTensor)

    # Latin hypercube sampling
    elif type_of_points == "lhs":
        return torch.from_numpy(lhs(dim, samples=n, criterion='center')).type(torch.FloatTensor)

    # Gauss-Legendre quadrature points
    elif type_of_points == "gauss":
        if n != 0:
            x, _ = np.polynomial.legendre.leggauss(n)
            x = 0.5 * (x.reshape(-1, 1) + 1)

            if dim == 1:
                return torch.from_numpy(x).type(torch.FloatTensor)
            if dim == 2:
                x = x.reshape(-1, )
                x = np.transpose([np.repeat(x, len(x)), np.tile(x, len(x))])
                return torch.from_numpy(x).type(torch.FloatTensor)
        else:
            return torch.zeros([0, dim])

    # grid points
    elif type_of_points == "grid":
        if n != 0:
            x = np.linspace(0, 1, n + 2)
            x = x[1:-1].reshape(-1, 1)
            if dim == 1:
                return torch.from_numpy(x).type(torch.FloatTensor)
            if dim == 2:
                x = x.reshape(-1, )
                if not boundary:
                    x = np.transpose(
                        [np.tile(x, len(x)), np.repeat(x, len(x))])
                else:
                    x = np.concatenate([x.reshape(-1, 1), x.reshape(-1, 1)], 1)
                print(x)
                return torch.from_numpy(x).type(torch.FloatTensor)
        else:
            return torch.zeros([0, dim])

    # sobol points
    elif type_of_points == "sobol":
        skip = random_seed
        data = np.full((n, dim), np.nan)
        for j in range(n):
            seed = j + skip
            data[j, :], next_seed = sobol_seq.i4_sobol(dim, seed)
        return torch.from_numpy(data).type(torch.FloatTensor)

    else:
        raise ValueError()
