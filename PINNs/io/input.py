import shutil
import json
import torch


def read_json(path):
    """
    Reads a json from path in a dict from path, returns it.
    """
    with open(path, 'r') as f:
        config = json.load(f)

    return config


def init(config_, folder_path):
    """
    Moves the config file to the given folder and outputs all properties.
    """

    shutil.copyfile(config_, folder_path + '/config.json')
    config = read_json(config_)

    n_coll = int(config['n_coll'])
    n_u = int(config['n_u'])
    n_int = int(config['n_int'])
    model = str(config['model'])
    n_hidden_layers = int(config['hidden_layers'])
    n_neurons = int(config['neurons'])
    activation = str(config['activation'])
    optimizer = str(config['optimizer'])
    n_epochs = int(config['num_epochs'])
    max_iter = int(config['max_iter'])
    batch_size = config['batch_size']
    shuffle = config['shuffle']
    w_r = torch.FloatTensor(config['w_r'])
    w_vars = float(config['w_vars'])
    w_reg = float(config['w_reg'])
    loss_weights = [w_r, w_vars, w_reg]
    regularization_order = int(config['regularization_order'])

    return n_coll, n_u, n_int, model, n_hidden_layers, n_neurons, activation, optimizer, n_epochs, max_iter, batch_size, shuffle, loss_weights, regularization_order


def save_args(args, folder_path, name='args'):
    """
    Save the equation arguments to a json in the folder_path.
    """
    with open(folder_path + '/' + name + '.json', 'w') as f:
        json.dump(args, f)
