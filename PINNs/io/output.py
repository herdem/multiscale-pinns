import torch
import csv
import numpy as np


def save_model(model, path):
    """
    Saves a model to path. The state_dict version is safer.
    """
    torch.save(model, path + '/model_vanilla.pt')
    torch.save(model.state_dict(), path + '/model_state_dict.pt')


def save_data(header, row, path_to_data):
    """
    Saves data in header and row as csv file to path_to_data.
    """
    with open(path_to_data + '/data.csv', 'w', newline='') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(header)
        csv_writer.writerow(row)


def prepare_training_data(training_time, losses):
    """
    Returns header and row for csv as lists.
    """
    header = ['training_time', 'total_loss', 'unweighted_total_loss']
    header += [f'residual_loss_{i}' for i in range(
        1, losses[2].shape[0] + 1)]
    header.append('vars_loss')
    header.append('regularization_loss')
    row = [training_time, losses[0].detach().item(), losses[1].detach().item()]
    row += losses[2].cpu().detach().numpy().tolist()
    row.append(losses[3].detach().item())
    row.append(losses[4].detach().item())

    return header, row


def prepare_generalization_data(equation, model, extrema, path_to_plots):
    """
    Computes the generalization error and returns header and row for csv file.
    """
    try:
        L1_errors, rel_L1_errors, L2_errors, rel_L2_errors = equation.compute_generalization_error(
            model, extrema, path_to_plots)
    except:
        raise AttributeError()

    avg_L1_error = np.mean(L1_errors)
    avg_rel_L1_error = np.mean(rel_L1_errors)

    header = [f'L1_error_{i}' for i in range(
        1, equation.output_dimension + 1)]
    header.append('average_L1_error')
    header += [f'relative_L1_error_{i}' for i in range(
        1, equation.output_dimension + 1)]
    header.append('average_relative_L1_error')
    row = L1_errors.tolist()
    row.append(avg_L1_error)
    row += rel_L1_errors.tolist()
    row.append(avg_rel_L1_error)

    avg_L2_error = np.mean(L2_errors)
    avg_rel_L2_error = np.mean(rel_L2_errors)

    header += [f'L2_error_{i}' for i in range(
        1, equation.output_dimension + 1)]
    header.append('average_L2_error')
    header += [f'relative_L2_error_{i}' for i in range(
        1, equation.output_dimension + 1)]
    header.append('average_relative_L2_error')
    row += L2_errors.tolist()
    row.append(avg_L2_error)
    row += rel_L2_errors.tolist()
    row.append(avg_rel_L2_error)

    return header, row
