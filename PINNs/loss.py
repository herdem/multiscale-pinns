import torch
import torch.nn as nn


class Loss(nn.Module):
    """
    The PINN losses.
    """

    def __init__(self):
        super(Loss, self).__init__()

    def forward(self, equation, model, x_coll, x_u, u, x_b, u_b, order_regularizer):
        """
        Returns three losses: the PDE loss (for every output), the vars loss (every constraint) and the regularization loss.
        """
        u_pred_vars = list()
        u_train_vars = list()

        # apply constraints
        if x_b.shape[0] != 0:
            equation.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)
        if x_u.shape[0] != 0:
            equation.apply_ic(model, x_u, u, u_pred_vars, u_train_vars)

        u_pred_tot_vars = torch.cat(u_pred_vars, 0).to(equation.device)
        u_train_tot_vars = torch.cat(u_train_vars, 0).to(equation.device)

        assert not torch.isnan(u_pred_tot_vars).any()

        res = equation.compute_res(model, x_coll).to(equation.device)

        loss_res = torch.mean(abs(res) ** 2, -1)
        loss_vars = torch.mean(abs(u_pred_tot_vars - u_train_tot_vars) ** 2)
        loss_reg = regularization(model, order_regularizer)

        return loss_res, loss_vars, loss_reg


def regularization(model, p):
    """
    Compute regularization loss of model weights.
    """
    reg_loss = 0.
    for name, param in model.named_parameters():
        if 'weight' in name:
            reg_loss += torch.norm(param, p)

    return reg_loss
