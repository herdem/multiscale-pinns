import torch
import torch.nn as nn


class mPINN(nn.Module):
    """
    Wrapper for PINN models. Creates one PINN for every scale/output_dim.
    """

    def __init__(self, model_class, input_dim, output_dim, num_hidden_layers, num_neurons, activation_f):
        super(mPINN, self).__init__()
        for i in range(output_dim):
            self.add_module(f'model_{i}', model_class(
                input_dim, 1, num_hidden_layers, num_neurons, activation_f))
        self.output_dim = output_dim

    def forward(self, x):
        outputs = list()
        for child in self.children():
            outputs.append(child(x))
        return torch.cat(outputs, dim=1)
