from .PINN import PINN
from .mPINN import mPINN
from .PINN_resnet import PINN_resnet
from .PINN_densenet import PINN_densenet
from .dirichlet_model_integration import Dirichlet_Wrapper
from .io.input import read_json
import torch


def import_state_dict_model(path, eq, mpinn, model_wrapper, rescale, G=None):
    """
    Load a model from a state dict. path must contain the model and config.json
    """

    config = read_json(path + '/config.json')
    model = str(config['model'])
    n_hidden_layers = int(config['hidden_layers'])
    n_neurons = int(config['neurons'])
    activation = str(config['activation'])

    input_dim = eq.time_dimensions + eq.space_dimensions
    output_dim = eq.output_dimension

    if model == 'vanilla':
        model = PINN
    elif model == 'resnet':
        model = PINN_resnet
    elif model == 'densenet':
        model = PINN_densenet
    else:
        raise ValueError()

    if mpinn:
        model = mPINN(model, input_dim, output_dim,
                      n_hidden_layers, n_neurons, activation)
    else:
        model = model(input_dim, output_dim,
                      n_hidden_layers, n_neurons, activation)

    if model_wrapper:
        model = Dirichlet_Wrapper(model, eq, rescale, G)

    model.load_state_dict(torch.load(
        path + '/model_state_dict.pt', map_location=torch.device('cpu')))
    return model
