import torch
import numpy as np
from .generate_points import generate_points


class SquareDomain:
    """
    Implements a (multi-dimensional) square domain.
    """

    def __init__(self, output_dimension, time_dimensions, space_dimensions, list_of_BC, extrema, type_of_points, parameters_values=None, vel_wave=None):
        self.output_dimension = output_dimension
        self.time_dimensions = time_dimensions
        self.space_dimensions = space_dimensions
        self.list_of_BC = list_of_BC
        self.extrema = extrema if parameters_values is None else torch.cat(
            [extrema, parameters_values], 0)
        self.type_of_points = type_of_points
        self.input_dimensions = self.extrema.shape[0]
        self.extrema_0 = self.extrema[:, 0]
        self.extrema_f = self.extrema[:, 1]
        self.vel_wave = vel_wave

        self.BC = list()

    def add_collocation_points(self, n_coll, random_seed):
        x_coll = generate_points(
            n_coll, self.input_dimensions, random_seed, self.type_of_points, False)
        x_coll = x_coll * (self.extrema_f - self.extrema_0) + self.extrema_0
        y_coll = torch.full((n_coll, self.output_dimension), np.nan)

        return x_coll, y_coll

    def add_boundary_points(self, n_boundary, random_seed):
        x_b_list = list()
        y_b_list = list()

        for i in range(self.time_dimensions, self.time_dimensions + self.space_dimensions):
            BC = list()
            val_0 = np.delete(self.extrema, i, 0)[:, 0]
            val_f = np.delete(self.extrema, i, 0)[:, 1]

            x_boundary_0 = generate_points(
                n_boundary, self.input_dimensions, random_seed, self.type_of_points, True)
            x_boundary_0[:, i] = torch.full(size=(n_boundary,), fill_value=0.0)
            x_boundary_0_wo_i = np.delete(x_boundary_0, i, 1)
            [y_boundary_0, type_BC] = self.list_of_BC[i -
                                                      self.time_dimensions][0](x_boundary_0_wo_i * (val_f - val_0) + val_0)
            BC.append(type_BC)
            x_b_list.append(x_boundary_0)
            y_b_list.append(y_boundary_0)

            x_boundary_1 = generate_points(
                n_boundary, self.input_dimensions, random_seed, self.type_of_points, True)
            x_boundary_1[:, i] = torch.full(size=(n_boundary,), fill_value=1.0)
            x_boundary_1_wo_i = np.delete(x_boundary_1, i, 1)
            [y_boundary_1, type_BC] = self.list_of_BC[i -
                                                      self.time_dimensions][1](x_boundary_1_wo_i * (val_f - val_0) + val_0)
            BC.append(type_BC)
            x_b_list.append(x_boundary_1)
            y_b_list.append(y_boundary_1)

            self.BC.append(BC)

        x_b = torch.cat(x_b_list, 0)
        y_b = torch.cat(y_b_list, 0)

        x_b = x_b * (self.extrema_f - self.extrema_0) + self.extrema_0

        return x_b, y_b

    def apply_bc(self, model, x_b, u_b, u_pred_var_list, u_train_var_list):
        for j in range(self.output_dimension):
            for i in range(self.space_dimensions):
                half_len_x_b_i = x_b.shape[0] // (2 * self.space_dimensions)
                x_b_i = x_b[i * (x_b.shape[0] // self.space_dimensions):(i + 1) * (x_b.shape[0] // self.space_dimensions), :]
                u_b_i = u_b[i * (x_b.shape[0] // self.space_dimensions):(i + 1) * (x_b.shape[0] // self.space_dimensions), :]

                boundary = 0
                while boundary < 2:
                    x_half_1 = x_b_i[half_len_x_b_i *
                                     boundary:half_len_x_b_i * (boundary + 1), :]
                    u_b_i_half = u_b_i[half_len_x_b_i *
                                       boundary:half_len_x_b_i * (boundary + 1), :]

                    x_half_2 = x_b_i[half_len_x_b_i *
                                     (boundary + 1):half_len_x_b_i * (boundary + 2), :]

                    boundary_conditions = self.BC[i][boundary][j]
                    boundary = boundary_conditions.apply(model, x_half_1, u_b_i_half, j, u_pred_var_list, u_train_var_list,
                                                         space_dim=i, x_boundary_sym=x_half_2, boundary=boundary, vel_wave=self.vel_wave)
                    boundary += 1
