# Physics-informed Neural Networks for Multiscale Differential Equations

This is the repository corresponding to my Bachelor's thesis with Prof. Mishra and Roberto Molinaro @ SAM, D-MATH, ETH Zurich.

The code is based upon Roberto Molinaro's research code but is refactored and extended.

## Structure

> The __analytics__ subfolder contains functions to collect data from ensemble trainings and plot solutions after training.

> In the __equations__ subfolder, all used equations are declared.

> The __PINNs__ subfolder contains classes and functions used to define and train PINNs.

> __simple_train.py__ and __ensemble_train.py__ are the drivers to train the PINNs in conjunction with the __.json__ files.

> The Jupyter notebooks were used to create plots and analyze the data.

> __other_code__ contains deprecated code or stuff that is not assignable to other folders.