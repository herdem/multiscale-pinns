import os
import pandas as pd
import pathlib
import torch
import numpy as np
import json
from PINNs.other import import_state_dict_model


def get_subfolders(x):
    """
    Get all subfolders excluding links from x.
    """
    return [d for d in os.listdir(
        x) if os.path.isdir(x + '/' + d) and not os.path.islink(x + '/' + d)]


def collect_ensemble_data(path_to_ensemble):
    """
    Collect all ensemble data from path_to_ensemble and return it as dataframe.
    """
    # go over all setups
    data = list()
    setup_paths = get_subfolders(path_to_ensemble)
    for setup_path in setup_paths:
        absolute_setup_path = path_to_ensemble + '/' + setup_path

        # go over all retrainings
        retrain_paths = get_subfolders(absolute_setup_path)
        retrain_data = list()
        for retrain in retrain_paths:
            retrain_id = int(retrain.split('_')[-1])
            retrain_path = absolute_setup_path + '/' + retrain
            if os.path.isfile(retrain_path + '/' + 'data.csv'):
                retrain_data_ = pd.read_csv(
                    retrain_path + '/data.csv', header=0)
                retrain_data_['retrain_id'] = retrain_id
                retrain_data.append(retrain_data_)
            else:
                print('No data file found in ' + retrain_path)
        retrain_data = pd.concat(retrain_data, ignore_index=True)

        with open(absolute_setup_path + '/config.json', 'r') as f:
            w_r = json.loads(f.read())
        w_r = pd.json_normalize(w_r, record_path=['w_r'])
        setup_config = pd.read_json(
            absolute_setup_path + '/config.json')
        setup_config.drop('w_r', inplace=True, axis=1)
        for i in range(len(w_r)):
            setup_config[f'w_r_{i}'] = w_r.iloc[i, 0]
        if setup_config['batch_size'].values[0] == 'full':
            setup_config['batch_size'] = setup_config['n_coll'] + \
                setup_config['n_u'] + setup_config['n_int']
        setup_id = int(setup_path.split('_')[-1])
        setup_config['setup_id'] = setup_id
        setup_retrain = pd.merge(setup_config.reset_index(
            drop=True), retrain_data.reset_index(drop=True), how='cross')
        data.append(setup_retrain)

    data = pd.concat(data, ignore_index=True)
    return data


def collect_ensemble_data_compute_other_scale(path_to_ensemble, collocation_points, naive_eq, naive_mpinn, naive_model_wrapper, naive_rescale, other_scale, exact, eval_points, eq_other_scale, ic=None, bc=None, G=None):
    """
    To be used to filter out a scale from a naive implementation.
    Adds relative_L1 and relative_L2 errors marked as other_scale.
    other scale is a function, that 
    Collect all ensemble data from path_to_ensemble, compute a scale's residual and return it as dataframe.
    """
    exact_sol_eval = exact(eval_points).detach().numpy().reshape(-1, 1)
    # go over all setups
    data = list()
    setup_paths = get_subfolders(path_to_ensemble)
    for setup_path in setup_paths:
        absolute_setup_path = path_to_ensemble + '/' + setup_path

        # go over all retrainings
        retrain_paths = get_subfolders(absolute_setup_path)
        retrain_data = list()
        for retrain in retrain_paths:
            retrain_id = int(retrain.split('_')[-1])
            retrain_path = absolute_setup_path + '/' + retrain
            if os.path.isfile(retrain_path + '/' + 'data.csv'):
                retrain_data_ = pd.read_csv(
                    retrain_path + '/data.csv', header=0)
                retrain_data_['retrain_id'] = retrain_id
                model = import_state_dict_model(
                    retrain_path, naive_eq, naive_mpinn, naive_model_wrapper, naive_rescale, G)
                model.eval()

                def filtered_scale(x):
                    return other_scale(model, x)

                res = eq_other_scale.compute_res(
                    filtered_scale, collocation_points)
                res = torch.mean(abs(res) ** 2, -1)
                u_pred_vars = list()
                u_train_vars = list()
                if ic != None:
                    eq_other_scale.apply_ic(
                        filtered_scale, ic[0], ic[1], u_pred_vars, u_train_vars)
                if bc != None:
                    eq_other_scale.apply_bc(
                        filtered_scale, bc[0], bc[1], u_pred_vars, u_train_vars)
                u_pred_tot_vars = torch.cat(u_pred_vars, 0)
                u_train_tot_vars = torch.cat(u_train_vars, 0)
                loss_vars = torch.mean(
                    abs(u_pred_tot_vars - u_train_tot_vars) ** 2)

                scale = (filtered_scale(eval_points)[
                         :, 0]).detach().numpy().reshape(-1, 1)
                rel_L1 = np.mean(abs(scale - exact_sol_eval)) / \
                    np.mean(abs(exact_sol_eval))
                rel_L2 = np.sqrt(np.mean((scale - exact_sol_eval) ** 2)
                                 ) / np.sqrt(np.mean(exact_sol_eval ** 2))
                retrain_data_[
                    'residual_loss_other_scale'] = res.detach().item()
                retrain_data_[
                    'vars_loss_other_scale'] = loss_vars.detach().item()
                retrain_data_['unweighted_total_loss_other_scale'] = retrain_data_[
                    'residual_loss_other_scale'] + retrain_data_['vars_loss_other_scale']
                retrain_data_['relative_L1_error_other_scale'] = rel_L1
                retrain_data_['relative_L2_error_other_scale'] = rel_L2
                retrain_data.append(retrain_data_)
            else:
                print('No data file found in ' + retrain_path)

        retrain_data = pd.concat(retrain_data, ignore_index=True)

        with open(absolute_setup_path + '/config.json', 'r') as f:
            w_r = json.loads(f.read())
        w_r = pd.json_normalize(w_r, record_path=['w_r'])
        setup_config = pd.read_json(
            absolute_setup_path + '/config.json')
        setup_config.drop('w_r', inplace=True, axis=1)
        for i in range(len(w_r)):
            setup_config[f'w_r_{i}'] = w_r.iloc[i, 0]
        if setup_config['batch_size'].values[0] == 'full':
            setup_config['batch_size'] = setup_config['n_coll'] + \
                setup_config['n_u'] + setup_config['n_int']
        setup_id = int(setup_path.split('_')[-1])
        setup_config['setup_id'] = setup_id
        setup_retrain = pd.merge(setup_config.reset_index(
            drop=True), retrain_data.reset_index(drop=True), how='cross')
        data.append(setup_retrain)

    data = pd.concat(data, ignore_index=True)
    return data


def stash_retrainings(data, criterion, mode='min'):
    """
    Stash the retrainings in data according to criterion and mode (min or mean).
    Return the stashed data.
    """
    setup_list = list()

    setups = data['setup_id'].unique()
    for setup in setups:
        retrainings = data.loc[data['setup_id'] == setup]
        retrainings = retrainings.sort_values(criterion)

        if mode == 'min':
            setup_list.append(retrainings.iloc[:1])
        elif mode == 'mean':
            retrain_id = retrainings['retrain_id'].iloc[0]
            retrainings = retrainings.mean()
            retrainings['retrain_id'] = retrain_id
            setup_list.append(retrainings)
        else:
            raise ValueError()

    return pd.concat(setup_list, ignore_index=True)


def get_best_setup(data, selection_criterion, path=None):
    """
    Return data of the best setup according to selection_criterion.
    If path is not None, will create symlink to it at path.
    """
    data = data.sort_values(selection_criterion)
    best = data.iloc[:1]
    best_setup = best['setup_id'].values[0]
    best_retrain = best['retrain_id'].values[0]

    if path is not None:
        path_ = path + '/setup_' + \
            str(best_setup) + '/retrain_' + str(best_retrain) + '/'
        if not os.path.islink(path + '/best_setup'):
            os.symlink(str(pathlib.Path().resolve()) + '/' +
                       path_, path + '/best_setup')

    return best


def collect_ensemble_data_compute_full_gen(path_to_ensemble, eq_sep, mpinn, wrapper, rescale, x, exact, G=None):
    """

    """
    exact_sol = exact(x)
    if len(list(exact_sol.size())) == 1:
        exact_sol = exact_sol.detach().numpy()
    else:
        exact_sol = exact_sol[:, 0].detach().numpy()
    # go over all setups
    data = list()
    setup_paths = get_subfolders(path_to_ensemble)
    for setup_path in setup_paths:
        absolute_setup_path = path_to_ensemble + '/' + setup_path

        # go over all retrainings
        retrain_paths = get_subfolders(absolute_setup_path)
        retrain_data = list()
        for retrain in retrain_paths:
            retrain_id = int(retrain.split('_')[-1])
            retrain_path = absolute_setup_path + '/' + retrain
            if os.path.isfile(retrain_path + '/' + 'data.csv'):
                retrain_data_ = pd.read_csv(
                    retrain_path + '/data.csv', header=0)
                retrain_data_['retrain_id'] = retrain_id
                model = import_state_dict_model(
                    retrain_path, eq_sep, mpinn, wrapper, rescale, G)
                model.eval()
                full_sol = eq_sep.get_full_solution(model, x).detach().numpy()
                L1_error = np.mean(abs(exact_sol - full_sol))
                rel_L1_error = L1_error / np.mean(abs(exact_sol))
                L2_error = np.sqrt(np.mean((exact_sol - full_sol) ** 2))
                rel_L2_error = L2_error / np.sqrt(np.mean(exact_sol ** 2))
                retrain_data_['L1_error_full'] = L1_error
                retrain_data_['L2_error_full'] = L2_error
                retrain_data_['relative_L1_error_full'] = rel_L1_error
                retrain_data_['relative_L2_error_full'] = rel_L2_error

                retrain_data.append(retrain_data_)
            else:
                print('No data file found in ' + retrain_path)

        retrain_data = pd.concat(retrain_data, ignore_index=True)

        with open(absolute_setup_path + '/config.json', 'r') as f:
            w_r = json.loads(f.read())
        w_r = pd.json_normalize(w_r, record_path=['w_r'])
        setup_config = pd.read_json(
            absolute_setup_path + '/config.json')
        setup_config.drop('w_r', inplace=True, axis=1)
        for i in range(len(w_r)):
            setup_config[f'w_r_{i}'] = w_r.iloc[i, 0]
        if setup_config['batch_size'].values[0] == 'full':
            setup_config['batch_size'] = setup_config['n_coll'] + \
                setup_config['n_u'] + setup_config['n_int']
        setup_id = int(setup_path.split('_')[-1])
        setup_config['setup_id'] = setup_id
        setup_retrain = pd.merge(setup_config.reset_index(
            drop=True), retrain_data.reset_index(drop=True), how='cross')
        data.append(setup_retrain)

    data = pd.concat(data, ignore_index=True)
    return data


def compute_whole_generalization_error(data, eq_sep, mpinn, wrapper, rescale, path_to_ensemble, x, exact, G=None):
    """
    Use this to comput the generalization error of the whole solution.
    """
    exact_sol = exact(x)[:, 0].detach().numpy()
    L1_errors, rel_L1_errors, L2_errors, rel_L2_errors = list(), list(), list(), list()
    for row in range(len(data)):
        model = import_state_dict_model(path_to_ensemble + '/setup_' + str(
            data.iloc[row]['setup_id']) + '/retrain_' + str(data.iloc[row]['retrain_id']), eq_sep, mpinn, wrapper, rescale, G)
        full_sol = eq_sep.get_full_solution(model, x).detach().numpy()

        L1_error = np.mean(abs(exact_sol - full_sol))
        L1_errors.append(L1_error)
        rel_L1_error = L1_error / np.mean(abs(exact_sol))
        rel_L1_errors.append(rel_L1_error)

        L2_error = np.sqrt(np.mean((exact_sol - full_sol) ** 2))
        L2_errors.append(L2_error)
        rel_L2_error = L2_error / np.sqrt(np.mean(exact_sol ** 2))
        rel_L2_errors.append(rel_L2_error)

    data['L1_error_full'] = L1_errors
    data['relative_L1_error_full'] = rel_L1_errors
    data['L2_error_full'] = L2_errors
    data['relative_L2_error_full'] = rel_L2_errors


def compute_whole_generalization_error_from_small(data, full_fn, eq_small, mpinn, wrapper, rescale, path_to_ensemble, x, exact, G=None):
    """
    Compute the generalization error of the full solution when only the small scale was learned.
    """
    exact_sol = exact(x)[:, 0].detach().numpy()
    L1_errors, rel_L1_errors, L2_errors, rel_L2_errors = list(), list(), list(), list()
    for row in range(len(data)):
        model = import_state_dict_model(path_to_ensemble + '/setup_' + str(
            data.iloc[row]['setup_id']) + '/retrain_' + str(data.iloc[row]['retrain_id']), eq_small, mpinn, wrapper, rescale, G)

        full_sol = full_fn(model, x)[:, 0].detach().numpy()

        L1_error = np.mean(abs(exact_sol - full_sol))
        L1_errors.append(L1_error)
        rel_L1_error = L1_error / np.mean(abs(exact_sol))
        rel_L1_errors.append(rel_L1_error)

        L2_error = np.sqrt(np.mean((exact_sol - full_sol) ** 2))
        L2_errors.append(L2_error)
        rel_L2_error = L2_error / np.sqrt(np.mean(exact_sol ** 2))
        rel_L2_errors.append(rel_L2_error)

    data['L1_error_full'] = L1_errors
    data['relative_L1_error_full'] = rel_L1_errors
    data['L2_error_full'] = L2_errors
    data['relative_L2_error_full'] = rel_L2_errors
