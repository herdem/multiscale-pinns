import pandas as pd


def filter_smaller(data, column, value):
    """
    Filter data such that all values in column are smaller than value and return df.
    """

    return data[data[column] < value]
