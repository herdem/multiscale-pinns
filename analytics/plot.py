import os
import torch
from PINNs.aesthetics import *
from PINNs.other import import_state_dict_model


def plot_solution(equation, model_path, mpinn, model_wrapper, rescale):
    """
    Plots the solution of an equation whose trained model is in model_path.
    """
    plot_path = model_path + '/plots'
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)

    model = import_state_dict_model(
        model_path, equation, mpinn, model_wrapper, rescale)
    equation.plot(model, plot_path, equation.extrema)


def plot_scatter_errors(data, selection_criterion, evaluation_criterion, folder, xlog=False, ylog=False):
    """
    Scatter plot selection_criterion vs evaluation_criterion, save in folder.
    """
    plt.figure()
    plt.grid(True, which='both', ls=':')
    plt.scatter(data[selection_criterion], data[evaluation_criterion])
    plt.xlabel(r'$\varepsilon_T$')
    plt.ylabel(r'$\varepsilon_G$')
    if ylog:
        plt.yscale('log')
    if xlog:
        plt.xscale('log')
    plt.savefig(folder + '/et_vs_eg.pdf', bbox_inches='tight')


def plot_sensitivity(data, col, label, criterion, folder, line='mean', xlim=None, xlog=False):
    """
    Plot a histplot with KDE from col of data over criterion.
    line: 'mean' or 'median'
    """
    values = data[col].values
    values = list(set(values))
    values.sort()

    df_values = list()
    for value in values:
        indices = data.index[data[col] == value]
        df_values.append(data.loc[indices])

    figure = plt.figure()
    axes = plt.gca()
    plt.grid(True, which='both', ls=':')

    i = 0
    for sensitivity in df_values:
        value = sensitivity[col].values[0]
        label_ = label + " = " + str(value).replace('_', '-')

        plot_var = sensitivity[criterion]
        if line == 'mean':
            l = plot_var.mean()
        elif line == 'median':
            l = plot_var.median()
        else:
            raise ValueError()
        if xlog:
            plot_var = np.log10(plot_var)
            l = np.log10(l)
        sns.distplot(plot_var, label=label_, kde=True,
                     hist=True, norm_hist=False, kde_kws={'linewidth': 2})
        kdeline = axes.lines[i]
        xs = kdeline.get_xdata()
        ys = kdeline.get_ydata()
        height = np.interp(l, xs, ys)
        axes.vlines(l, 0, height, color=axes.get_lines()
                    [-1].get_c(), ls=':', lw=2)
        axes.fill_between(xs, 0, ys, facecolor=axes.get_lines()
                          [-1].get_c(), alpha=0.2)
        i += 1

    if xlim != None:
        plt.xlim(xlim)
    if xlog:
        plt.xlabel(r'$\log_{10}(\varepsilon_G)$')
    else:
        plt.xlabel(r'$\varepsilon_G$')
    plt.legend(loc=1)
    plt.savefig(folder + '/sensitivity_' +
                col + ".pdf", bbox_inches='tight')
