"""
Does ensemble training on PINNs using parameters given in config.
Usage: python3 ensemble_train.py path_to_config folder_path seed compute_generalization_error_bool plot_bool on_cluster_bool log_bool
"""

import os
import sys
import itertools
import json
import random
import subprocess

import PINNs.io.input as input

if len(sys.argv) != 8:
    raise ValueError()

path_to_config = str(sys.argv[1])
with open(path_to_config, 'r') as f:
    config = json.load(f)
folder_path = str(sys.argv[2])
seed = int(sys.argv[3])
compute_generalization_error = sys.argv[4]
plot = sys.argv[5]
if str(sys.argv[6]) == 'true':
    on_cluster = True
else:
    on_cluster = False

log = sys.argv[7]

if os.path.isdir(folder_path):
    print('Directory already exists. Abort.')
    exit()

os.mkdir(folder_path)

# settings
# GPU="GeForceGTX1080"  # GPU = "GeForceGTX1080Ti"  # GPU = "TeslaV100_SXM2_32GB"
GPU = 'NVIDIAGeForceGTX1080'
n_retrain = 5

random.seed(seed)

settings = list(itertools.product(*config.values()))
i = 0
for setup in settings:
    setup_path = folder_path + '/setup_' + str(i)
    os.mkdir(setup_path)
    setup_config = {
        "n_coll": setup[0],
        "n_u": setup[1],
        "n_int": setup[2],
        "model": setup[3],
        "hidden_layers": setup[4],
        "neurons": setup[5],
        "activation": setup[6],
        "optimizer": setup[7],
        "num_epochs": setup[8],
        "max_iter": setup[9],
        "batch_size": setup[10],
        "shuffle": setup[11],
        "regularization_order": setup[12],
        "w_r": setup[13],
        "w_vars": setup[14],
        "w_reg": setup[15]
    }
    input.save_args(setup_config, setup_path, name='config')
    i += 1

    for retrain in range(n_retrain):
        retrain_path = setup_path + '/retrain_' + str(retrain)
        seed = random.randint(1, 10000)
        arguments = [setup_path + '/config.json', retrain_path,
                     str(seed), compute_generalization_error, plot, log]

        if sys.platform == "linux" or sys.platform == "linux2" or sys.platform == "darwin":
            if on_cluster:
                if GPU != 'None':
                    exec_string = 'bsub -n 1 -We 4:00 -R \"rusage[mem=8192,ngpus_excl_p=1]\" -R \"select[gpu_model0==' + \
                        GPU + ']\" python3 simple_train.py '
                else:
                    exec_string = 'bsub -n 1 -We 4:00 -R \"rusage[mem=4096]\" python3 simple_train.py '
            else:
                exec_string = 'python3 simple_train.py '

            for arg in arguments:
                exec_string += ' ' + arg

            os.system(exec_string)
        else:
            python = os.environ['PYTHON36']
            p = subprocess.Popen([python, "PINNS2.py"] + arguments)
            p.wait()
