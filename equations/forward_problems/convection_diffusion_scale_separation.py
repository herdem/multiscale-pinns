from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from torchdiffeq import odeint
from PINNs.boundary_conditions import DirichletBC


class Convection_Diffusion_scale_separation(Equation):
    """
    Defines a 1D steady convection-diffusion equation.
    """

    def __init__(self, c):
        super(Convection_Diffusion_scale_separation, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 2
        self.space_dimensions = 1
        self.time_dimensions = 0
        self.extrema = torch.tensor([[0, 1]])
        self.list_of_BC = list([[self.T_0b, self.T_1b]])
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.c = c  # control the scales

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_boundary_points(self, n_b, random_seed):
        return self.domain.add_boundary_points(n_b, random_seed)

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        self.domain.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)

    def T_0b(self, x):
        type_BC = [DirichletBC(), DirichletBC()]
        T = torch.full(size=(x.shape[0], 2), fill_value=0.)
        return T, type_BC

    def T_1b(self, x):
        type_BC = [DirichletBC(), DirichletBC()]
        T = torch.tensor([[1., -1. / self.c]])
        return T, type_BC

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        T = model(x_coll)
        T_0 = T[:, 0]
        T_1 = T[:, 1]

        T_0_x = torch.autograd.grad(
            T_0, x_coll, grad_outputs=torch.ones_like(T_0), create_graph=True)[0][:, 0]
        T_1_x = torch.autograd.grad(
            T_1, x_coll, grad_outputs=torch.ones_like(T_1), create_graph=True)[0][:, 0]
        T_0_xx = torch.autograd.grad(T_0_x, x_coll, grad_outputs=torch.ones_like(
            T_0_x), create_graph=True)[0][:, 0].reshape(-1,)
        T_1_xx = torch.autograd.grad(T_1_x, x_coll, grad_outputs=torch.ones_like(
            T_1_x), create_graph=True)[0][:, 0].reshape(-1,)

        res_1 = T_0_x - torch.ones_like(T_0_x)
        res_2 = self.c * T_1_x - (T_0_xx / self.c + T_1_xx)

        return torch.stack([res_1, res_2])

    def get_full_solution(self, model, x):
        model.eval()
        sol = model(x)
        return sol[:, 0] + self.c * sol[:, 1]

    def exact(self, x):
        c = torch.tensor(self.c)
        y1 = torch.log(torch.exp(c * (x - 1)) - torch.exp(-c))
        y2 = torch.log(1. - torch.exp(-c))
        return torch.stack([x, -1. / self.c * torch.exp(y1 - y2)], 1)

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = self.convert(torch.rand([100000, extrema.shape[0]]), extrema)
        exact = (self.exact(x)).numpy()
        out = model(x).detach().numpy()
        L1_errors = list()
        rel_L1_errors = list()
        L2_errors = list()
        rel_L2_errors = list()
        for i in range(self.output_dimension):
            exact_i = exact[:, i].reshape(-1, 1)
            out_i = out[:, i].reshape(-1, 1)
            assert exact_i.shape[1] == out_i.shape[1]
            L1_error_i = np.mean(abs(exact_i - out_i))
            L1_errors.append(L1_error_i)
            L2_error_i = np.sqrt(np.mean((exact_i - out_i) ** 2))
            L2_errors.append(L2_error_i)
            rel_L1_error_i = L1_error_i / np.mean(abs(exact_i))
            rel_L1_errors.append(rel_L1_error_i)
            rel_L2_error_i = L2_error_i / np.sqrt(np.mean(exact_i ** 2))
            rel_L2_errors.append(rel_L2_error_i)

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact[:, 0].reshape(-1, 1),
                        out[:, 0].reshape(-1, 1), label=r'$u_{big}$')
            plt.scatter(exact[:, 1].reshape(-1, 1),
                        out[:, 1].reshape(-1, 1), label=r'$u_{small}$')
            plt.xlabel(r'exact values')
            plt.ylabel(r'predicted values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.asarray(L1_errors), np.asarray(rel_L1_errors), np.asarray(L2_errors), np.asarray(rel_L2_errors)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        x = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 1000), [1000, 1])

        exact = self.exact(x)
        sol = model(x).detach().numpy()

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(x, exact[:, 0],
                 linewidth=2, label=r'exact $T_0$')
        plt.plot(x, exact[:, 1],
                 linewidth=2, label=r'exact $T_1$')
        plt.scatter(x, sol[:, 0],
                    label=r'predicted $T_0$', marker="o", s=14)
        plt.scatter(x, sol[:, 1],
                    label=r'predicted $T_1$', marker="o", s=14)

        plt.xlabel(r'$x$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution_split.pdf")

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(x, exact[:, 0] + self.c * exact[:, 1],
                 linewidth=2, label=r'exact $T$')
        plt.scatter(x, self.get_full_solution(model, x).detach().numpy(),
                    label=r'predicted $T$', marker="o", s=14)

        plt.xlabel(r'$x$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")
