from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from PINNs.boundary_conditions import DirichletBC


class Convection_Diffusion_small_scale(Equation):
    """
    Defines a 1D steady convection-diffusion equation.
    """

    def __init__(self, c):
        super(Convection_Diffusion_small_scale, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 1
        self.space_dimensions = 1
        self.time_dimensions = 0
        self.extrema = torch.tensor([[0, 1]])
        self.list_of_BC = list([[self.T_0b, self.T_1b]])
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.c = c  # control the scales

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_boundary_points(self, n_b, random_seed):
        return self.domain.add_boundary_points(n_b, random_seed)

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        self.domain.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)

    def T_0b(self, x):
        type_BC = [DirichletBC()]
        T = torch.full(size=(x.shape[0], 1), fill_value=0.)
        return T, type_BC

    def T_1b(self, x):
        type_BC = [DirichletBC()]
        T = torch.tensor([[-1. / self.c]])
        return T, type_BC

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        T = model(x_coll)[:, 0]

        grad_T = torch.autograd.grad(
            T, x_coll, grad_outputs=torch.ones_like(T), create_graph=True)[0][:, 0]
        res = self.c * grad_T - torch.autograd.grad(grad_T, x_coll, grad_outputs=torch.ones_like(
            grad_T), create_graph=True)[0][:, 0].reshape(-1,)

        return torch.unsqueeze(res, 0)

    def exact(self, x):
        c = torch.tensor(self.c)
        y1 = torch.log(torch.exp(c * (x - 1)) - torch.exp(-c))
        y2 = torch.log(1. - torch.exp(-c))
        return - 1. / self.c * torch.exp(y1 - y2)

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = self.convert(torch.rand([100000, extrema.shape[0]]), extrema)
        exact = (self.exact(x)).numpy().reshape(-1, 1)
        out = model(x).detach().numpy()[:, 0].reshape(-1, 1)
        assert (exact.shape[1] == out.shape[1])

        L1_error = np.mean(abs(exact - out))
        rel_L1_error = L1_error / np.mean(abs(exact))

        L2_error = np.sqrt(np.mean((exact - out) ** 2))
        rel_L2_error = L2_error / np.sqrt(np.mean(exact ** 2))

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact, out, label=r'u')
            plt.xlabel(r'exact values')
            plt.ylabel(r'predicted values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.expand_dims(L1_error, axis=0), np.expand_dims(rel_L1_error, axis=0), np.expand_dims(L2_error, axis=0), np.expand_dims(rel_L2_error, axis=0)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        x = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 1000), [1000, 1])

        exact = self.exact(x)
        sol = model(x)[:, 0].detach().numpy()

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(x, exact,
                 linewidth=2, label=r'exact $T_1$')
        plt.scatter(x, sol,
                    label=r'predicted $T_1$', marker="o", s=14)

        plt.xlabel(r'$x$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")
