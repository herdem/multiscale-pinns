from venv import create
from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
from PINNs.generate_points import generate_points
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from torchdiffeq import odeint


class Duffing_Oscillator_scale_separation(Equation):
    """
    Defines the Duffing oscillator ODE.
    """

    def __init__(self, domain_end, c):
        super(Duffing_Oscillator_scale_separation, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 2
        self.space_dimensions = 0
        self.time_dimensions = 1
        self.extrema = torch.tensor([[0, domain_end]])
        self.list_of_BC = list()
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.c = c  # control the scales
        self.u0 = torch.tensor([1, 0])  # initial condition
        self.du0dt = torch.tensor([0, 0])  # initial condition

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_initial_points(self, n_init, random_seed):
        extrema_0 = self.extrema[:, 0]
        extrema_f = self.extrema[:, 1]
        x_time_0 = generate_points(n_init, self.time_dimensions +
                                   self.space_dimensions, random_seed, self.type_of_points, True)
        x_time_0[:, 0] = torch.full(size=(n_init,), fill_value=0.0)
        x_time_0 = x_time_0 * (extrema_f - extrema_0) + extrema_0

        return x_time_0, self.u0.repeat(n_init, 1)

    def apply_ic(self, model, x_u, u, u_pred_vars, u_train_vars):
        x_u.requires_grad = True
        for j in range(self.output_dimension):
            if x_u.shape[0] != 0:
                out = model(x_u)[:, j]
                grad_out = torch.autograd.grad(
                    out, x_u, grad_outputs=torch.ones_like(out), create_graph=True)[0][:, 0]
                u_pred_vars.append(out)
                u_pred_vars.append(grad_out)
                u_train_vars.append(u[:, j])
                u_train_vars.append(self.v0(x_u)[:, j])

    def v0(self, x):
        return self.du0dt.repeat(x.shape[0], 1)

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        u = model(x_coll)
        u_big = u[:, 0].reshape(-1,)
        u_small = u[:, 1].reshape(-1,)

        grad_u_big_t = torch.autograd.grad(
            u_big, x_coll, grad_outputs=torch.ones_like(u_big), create_graph=True)[0][:, 0]
        grad_u_small_t = torch.autograd.grad(
            u_small, x_coll, grad_outputs=torch.ones_like(u_small), create_graph=True)[0][:, 0]

        grad_u_big_tt = torch.autograd.grad(grad_u_big_t, x_coll, grad_outputs=torch.ones_like(
            grad_u_big_t), create_graph=True)[0][:, 0]
        grad_u_small_tt = torch.autograd.grad(grad_u_small_t, x_coll, grad_outputs=torch.ones_like(
            grad_u_small_t), create_graph=True)[0][:, 0]

        res_big = grad_u_big_tt + u_big
        res_small = grad_u_small_tt + u_small + (self.c * u_small + u_big) ** 3

        return torch.stack([res_big, res_small])

    def get_full_solution(self, model, t):
        model.eval()
        sol = model(t)
        return sol[:, 0] + self.c * sol[:, 1]

    def integrated_sol(self, input):
        def f(t, y):
            y_big = torch.Tensor([y[0]])
            z_big = torch.Tensor([y[1]])
            y_small = torch.Tensor([y[2]])
            z_small = torch.Tensor([y[3]])
            return torch.cat([z_big, -y_big, z_small, -y_small - (y_big + self.c * y_small) ** 3])

        return odeint(f, torch.stack([self.u0[0], self.du0dt[0], self.u0[1], self.du0dt[1]]).float(), torch.flatten(input))[:, ::2]

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 100000), [100000, 1])
        exact = (self.integrated_sol(x)).numpy()
        out = model(x).detach().numpy()
        L1_errors = list()
        rel_L1_errors = list()
        L2_errors = list()
        rel_L2_errors = list()
        for i in range(self.output_dimension):
            exact_i = exact[:, i].reshape(-1, 1)
            out_i = out[:, i].reshape(-1, 1)
            assert exact_i.shape[1] == out_i.shape[1]
            L1_error_i = np.mean(abs(exact_i - out_i))
            L1_errors.append(L1_error_i)
            L2_error_i = np.sqrt(np.mean((exact_i - out_i) ** 2))
            L2_errors.append(L2_error_i)
            rel_L1_error_i = L1_error_i / np.mean(abs(exact_i))
            rel_L1_errors.append(rel_L1_error_i)
            rel_L2_error_i = L2_error_i / np.sqrt(np.mean(exact_i ** 2))
            rel_L2_errors.append(rel_L2_error_i)

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact[:, 0].reshape(-1, 1),
                        out[:, 0].reshape(-1, 1), label=r'$u_{big}$')
            plt.scatter(exact[:, 1].reshape(-1, 1),
                        out[:, 1].reshape(-1, 1), label=r'$u_{small}$')
            plt.xlabel(r'exact values')
            plt.ylabel(r'predicted values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.asarray(L1_errors), np.asarray(rel_L1_errors), np.asarray(L2_errors), np.asarray(rel_L2_errors)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        t = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 1000), [1000, 1])

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(t, self.integrated_sol(t)[:, 0],
                 linewidth=2, label=r'integrated $u_0$')
        plt.scatter(t, model(t)[:, 0].detach().numpy(),
                    label=r'predicted $u_0$', marker="o", s=14)
        plt.plot(t, self.integrated_sol(t)[:, 1],
                 linewidth=2, label=r'integrated $u_1$')
        plt.scatter(t, model(t)[:, 1].detach().numpy(),
                    label=r'predicted $u_1$', marker="o", s=14)

        plt.xlabel(r'$t$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution_split.pdf")

        plt.figure()
        plt.grid(True, which="both", ls=":")
        int_sol = self.integrated_sol(t)
        plt.plot(t, int_sol[:, 0] + self.c * int_sol[:, 1],
                 linewidth=2, label=r'integrated $u$')
        plt.scatter(t, self.get_full_solution(model, t).detach().numpy(),
                    label=r'predicted $u$', marker="o", s=14)

        plt.xlabel(r'$t$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")
