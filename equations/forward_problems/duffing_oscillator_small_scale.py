from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
from PINNs.generate_points import generate_points
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from torchdiffeq import odeint


class Duffing_Oscillator_small_scale(Equation):
    """
    Defines the small scale of the Duffing oscillator ODE.
    """

    def __init__(self, domain_end, c, on_unit=False):
        super(Duffing_Oscillator_small_scale, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 1
        self.space_dimensions = 0
        self.time_dimensions = 1
        if on_unit:
            self.extrema = torch.tensor([[0, 1]])
        else:
            self.extrema = torch.tensor([[0, domain_end]])
        self.list_of_BC = list()
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.on_unit = on_unit
        self.domain_end = domain_end
        self.c = c  # control the scales
        self.u0 = torch.tensor(0)  # initial condition
        self.du0dt = torch.tensor(0)  # initial condition

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_initial_points(self, n_init, random_seed):
        extrema_0 = self.extrema[:, 0]
        extrema_f = self.extrema[:, 1]
        x_time_0 = generate_points(n_init, self.time_dimensions +
                                   self.space_dimensions, random_seed, self.type_of_points, True)
        x_time_0[:, 0] = torch.full(size=(n_init,), fill_value=0.0)
        x_time_0 = x_time_0 * (extrema_f - extrema_0) + extrema_0

        return x_time_0, self.u0.repeat(n_init, 1)

    def apply_ic(self, model, x_u, u, u_pred_vars, u_train_vars):
        x_u.requires_grad = True
        for j in range(self.output_dimension):
            if x_u.shape[0] != 0:
                out = model(x_u)[:, j]
                grad_out = torch.autograd.grad(
                    out, x_u, grad_outputs=torch.ones_like(out), create_graph=True)[0][:, j]
                u_pred_vars.append(out)
                u_pred_vars.append(grad_out)
                u_train_vars.append(u[:, j])
                u_train_vars.append(self.v0(x_u)[:, j])

    def v0(self, x):
        return torch.full((x.shape[0], 1), fill_value=self.du0dt)

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        u = model(x_coll)[:, 0]
        grad_u = torch.autograd.grad(
            u, x_coll, grad_outputs=torch.ones_like(u), create_graph=True)[0][:, 0]
        grad_u_tt = torch.autograd.grad(grad_u, x_coll, grad_outputs=torch.ones_like(
            u), create_graph=True)[0][:, 0].reshape(-1,)

        if self.on_unit:
            res = grad_u_tt + (self.domain_end) ** 2 * (u + self.c *
                                                        (u + torch.cos(torch.squeeze(x_coll))) ** 3)
        else:
            res = grad_u_tt + u + \
                (self.c * u + torch.cos(torch.squeeze(x_coll))) ** 3
        return torch.unsqueeze(res, 0)

    def integrated_sol(self, input):
        def f(t, y):
            x = torch.Tensor([y[0]])
            dx_dt = torch.Tensor([y[1]])
            d2x_dt2 = torch.Tensor(
                [-(x + (self.c * x + torch.cos(t)) ** 3)])
            return torch.cat([dx_dt, d2x_dt2])

        return odeint(f, torch.stack([self.u0, self.du0dt]).float(), torch.flatten(input))[:, 0]

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 100000), [100000, 1])
        if self.on_unit:
            exact = (self.integrated_sol(self.domain_end * x)
                     ).numpy().reshape(-1, 1)
        else:
            exact = (self.integrated_sol(x)).numpy().reshape(-1, 1)
        out = model(x).detach().numpy()
        out = out[:, 0].reshape(-1, 1)
        assert (exact.shape[1] == out.shape[1])

        L1_error = np.mean(abs(exact - out))
        rel_L1_error = L1_error / np.mean(abs(exact))

        L2_error = np.sqrt(np.mean((exact - out) ** 2))
        rel_L2_error = L2_error / np.sqrt(np.mean(exact ** 2))

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact, out, label=r'u')
            plt.xlabel(r'Exact Values')
            plt.ylabel(r'Predicted Values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.expand_dims(L1_error, axis=0), np.expand_dims(rel_L1_error, axis=0), np.expand_dims(L2_error, axis=0), np.expand_dims(rel_L2_error, axis=0)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        t = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 1000), [1000, 1])

        if self.on_unit:
            integrated_sol = self.integrated_sol(self.domain_end * t)
        else:
            integrated_sol = self.integrated_sol(t)
        sol = model(t)[:, 0].detach().numpy()

        plt.figure()
        plt.grid(True, which="both", ls=":")
        if self.on_unit:
            plt.plot(self.domain_end * t, integrated_sol,
                     linewidth=2, label=r'integrated $u_1$')
            plt.scatter(self.domain_end * t, sol,
                        label=r'predicted $u_1$', marker="o", s=14)
        else:
            plt.plot(t, integrated_sol,
                     linewidth=2, label=r'integrated $u_1$')
            plt.scatter(t, sol,
                        label=r'predicted $u_1$', marker="o", s=14)

        plt.xlabel(r'$t$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")

        plt.figure()
        plt.grid(True, which="both", ls=":")
        if self.on_unit:
            plt.plot(self.domain_end * t, abs(integrated_sol - sol),
                     linewidth=2)
        else:
            plt.plot(t, abs(integrated_sol - sol), linewidth=2)
        plt.xlabel(r'$t$')
        plt.ylabel(r'$|u_{integrated} - u_{PINN}|$')
        plt.savefig(path_to_plots + "/error.pdf")
