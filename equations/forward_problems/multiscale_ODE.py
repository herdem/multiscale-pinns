from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
from PINNs.generate_points import generate_points
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *


class Multiscale_ODE(Equation):
    """
    Defines a two-dimensional simple multi-scale ODE.
    """

    def __init__(self, c):
        super(Multiscale_ODE, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 2
        self.space_dimensions = 0
        self.time_dimensions = 1
        self.extrema = torch.tensor([[0, 5]])
        self.list_of_BC = list()
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.c = c  # the size of the other scale
        self.u0 = torch.tensor([1, 1])  # initial conditions

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_initial_points(self, n_init, random_seed):
        extrema_0 = self.extrema[:, 0]
        extrema_f = self.extrema[:, 1]
        x_time_0 = generate_points(n_init, self.time_dimensions +
                                   self.space_dimensions, random_seed, self.type_of_points, True)
        x_time_0[:, 0] = torch.full(size=(n_init,), fill_value=0.0)
        x_time_0 = x_time_0 * (extrema_f - extrema_0) + extrema_0

        return x_time_0, self.u0.repeat(n_init, 1)

    def apply_ic(self, model, x_u, u, u_pred_vars, u_train_vars):
        for j in range(self.output_dimension):
            if x_u.shape[0] != 0:
                out = model(x_u)[:, j]
                u_pred_vars.append(out)
                u_train_vars.append(u[:, j])

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        u = model(x_coll)
        u_1 = u[:, 0]
        u_2 = u[:, 1]
        grad_u_1 = torch.autograd.grad(
            u_1, x_coll, grad_outputs=torch.ones_like(u_1), create_graph=True)[0].reshape(-1, )
        grad_u_2 = torch.autograd.grad(
            u_2, x_coll, grad_outputs=torch.ones_like(u_2), create_graph=True)[0].reshape(-1, )

        res_1 = grad_u_1 + u_1
        res_2 = self.c * grad_u_2 + u_2
        residual = torch.stack(
            [res_1, res_2])
        return residual

    def exact(self, x):
        exact = torch.cat(
            (self.u0[0] * torch.exp(-x), self.u0[1] * torch.exp(-x / self.c)), 1)
        return exact

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = self.convert(torch.rand([100000, extrema.shape[0]]), extrema)
        exact = (self.exact(x)).numpy()
        out = model(x).detach().numpy()
        L1_errors = list()
        rel_L1_errors = list()
        L2_errors = list()
        rel_L2_errors = list()
        for i in range(self.output_dimension):
            exact_i = exact[:, i].reshape(-1, 1)
            out_i = out[:, i].reshape(-1, 1)
            assert exact_i.shape[1] == out_i.shape[1]
            L1_error_i = np.mean(abs(exact_i - out_i))
            L1_errors.append(L1_error_i)
            L2_error_i = np.sqrt(np.mean((exact_i - out_i) ** 2))
            L2_errors.append(L2_error_i)
            rel_L1_error_i = L1_error_i / np.mean(abs(exact_i))
            rel_L1_errors.append(rel_L1_error_i)
            rel_L2_error_i = L2_error_i / np.sqrt(np.mean(exact_i ** 2))
            rel_L2_errors.append(rel_L2_error_i)

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact[:, 0].reshape(-1, 1),
                        out[:, 0].reshape(-1, 1), label=r'u')
            plt.scatter(exact[:, 1].reshape(-1, 1),
                        out[:, 1].reshape(-1, 1), label=r'v')
            plt.xlabel(r'exact Values')
            plt.ylabel(r'predicted Values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.asarray(L1_errors), np.asarray(rel_L1_errors), np.asarray(L2_errors), np.asarray(rel_L2_errors)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        t = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 100), [100, 1])

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(t, self.exact(t)[:, 0], 'b-',
                 linewidth=2, label=r'exact u', zorder=0)
        plt.scatter(t, model(t)[:, 0].detach().numpy(),
                    label=r'predicted u', marker="o", s=14, zorder=10)
        plt.plot(t, self.exact(t)[:, 1], 'r-',
                 linewidth=2, label=r'exact v', zorder=0)
        plt.scatter(t, model(t)[:, 1].detach().numpy(),
                    label=r'predicted v', marker="o", s=14, zorder=10)

        plt.xlabel(r'$t$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")
