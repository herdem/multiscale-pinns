from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from PINNs.boundary_conditions import DirichletBC, NoBC


class Navier_Stokes_big_scale(Equation):
    """
    Defines the 2D steady incompressible Navier Stokes equations.
    """

    def __init__(self, Re):
        super(Navier_Stokes_big_scale, self).__init__()

        self.type_of_points = 'sobol'
        self.output_dimension = 2
        self.space_dimensions = 2
        self.time_dimensions = 0
        self.extrema = torch.tensor([[0, 1], [0, 1]])
        self.list_of_BC = list([[self.x_b, self.x_b], [
                               self.lower_b, self.upper_b]])
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.Re = Re

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_boundary_points(self, n_b, random_seed):
        return self.domain.add_boundary_points(n_b, random_seed)

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        self.domain.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)

    def boundary_fn_u(self, x):
        return 1. / 0.995 * torch.nn.functional.relu(x - 0.995)

    def x_b(self, x):
        type_BC = [DirichletBC(), DirichletBC()]
        u = self.boundary_fn_u(x)
        v = torch.full(size=(x.shape[0], 1), fill_value=0.)
        return torch.cat([u, v], dim=-1), type_BC

    def lower_b(self, x):
        type_BC = [DirichletBC(), DirichletBC()]
        val = torch.full(size=(x.shape[0], 2), fill_value=0.)
        return val, type_BC

    def upper_b(self, x):
        type_BC = [DirichletBC(), DirichletBC()]
        u = torch.full(size=(x.shape[0], 1), fill_value=1.)
        v = torch.full(size=(x.shape[0], 1), fill_value=0.)
        return torch.cat([u, v], dim=-1), type_BC

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        out = model(x_coll)
        u_0 = out[:, 0]
        v_0 = out[:, 1]

        grad_u_0 = torch.autograd.grad(
            u_0, x_coll, grad_outputs=torch.ones_like(u_0), create_graph=True)[0]
        grad_u_0_x = grad_u_0[:, 0]
        grad_u_0_y = grad_u_0[:, 1]
        grad_v_0 = torch.autograd.grad(
            v_0, x_coll, grad_outputs=torch.ones_like(v_0), create_graph=True)[0]
        grad_v_0_x = grad_v_0[:, 0]
        grad_v_0_y = grad_v_0[:, 1]

        res_incomp_0 = grad_u_0_x + grad_v_0_y
        res_x_0 = u_0 * grad_u_0_x + v_0 * grad_u_0_y
        res_y_0 = u_0 * grad_v_0_x + v_0 * grad_v_0_y

        return torch.stack([res_incomp_0, res_x_0, res_y_0])

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        x = torch.linspace(
            extrema[0, 0], extrema[0, 1], 100)
        y = torch.linspace(
            extrema[1, 0], extrema[1, 1], 100)
        grid_x, grid_y = torch.meshgrid(x, y)

        plt.figure()
        plot_var = torch.cat(
            [torch.reshape(torch.flatten(grid_x), (100*100, 1)), torch.reshape(torch.flatten(grid_y), (100*100, 1))], 1)
        out = model(plot_var)
        u = out[:, 0].detach().numpy()
        v = out[:, 1].detach().numpy()

        u = u.reshape(100, 100).T
        v = v.reshape(100, 100).T
        speed = np.sqrt(u ** 2 + v ** 2)

        plt.contourf(x.detach().numpy(), y.detach(
        ).numpy(), speed)
        stream = plt.streamplot(x.detach().numpy(), y.detach(
        ).numpy(), u, v)
        plt.ylim(0, 1)

        plt.xlabel(r'$x$')
        plt.ylabel(r'$y$')
        cbar = plt.colorbar()
        cbar.set_label(r'speed')
        plt.savefig(path_to_plots + "/solution_velocity.pdf")
