from matplotlib import path
from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *
from PINNs.boundary_conditions import DirichletBC, NoBC
from PINNs.other import import_state_dict_model
from equations.forward_problems.navier_stokes_scale_separation_big_scale import Navier_Stokes_big_scale as big_scale


class Navier_Stokes_small_scale_refine(Equation):
    """
    Defines the 2D steady incompressible Navier Stokes equations.
    """

    def __init__(self, Re, path_to_best_big_scale):
        super(Navier_Stokes_small_scale_refine, self).__init__()

        self.type_of_points = 'sobol'
        self.output_dimension = 3
        self.space_dimensions = 2
        self.time_dimensions = 0
        self.extrema = torch.tensor([[0, 1], [0, 1]])
        self.list_of_BC = list([[self.boundary, self.boundary], [
                               self.boundary, self.boundary]])
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.Re = Re
        self.eq_big_scale = big_scale(Re=self.Re)
        self.model_big_scale = import_state_dict_model(
            path_to_best_big_scale, self.eq_big_scale, False, False, None)
        # don't fine-tune this scale
        for p in self.model_big_scale.parameters():
            p.requires_grad = False

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_boundary_points(self, n_b, random_seed):
        return self.domain.add_boundary_points(n_b, random_seed)

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        self.domain.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)

    def boundary(self, x):
        type_BC = [DirichletBC(), DirichletBC(), NoBC()]
        val = torch.full(size=(x.shape[0], 3), fill_value=0.)
        return val, type_BC

    def get_full_solution(self, model, x):
        model.eval()
        sol = model(x)
        self.model_big_scale.eval()
        sol_big = self.model_big_scale(x)
        return torch.stack([sol_big[:, 0] + self.Re * sol[:, 0], sol_big[:, 1] + self.Re * sol[:, 1], sol[:, 2]], 1)

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        out = model(x_coll)
        out_big_scale = self.model_big_scale(x_coll)
        u_0 = out_big_scale[:, 0]
        u_1 = out[:, 0]
        v_0 = out_big_scale[:, 1]
        v_1 = out[:, 1]
        p = out[:, 2]

        grad_u_0 = torch.autograd.grad(
            u_0, x_coll, grad_outputs=torch.ones_like(u_0), create_graph=True)[0]
        grad_u_0_x = grad_u_0[:, 0]
        grad_u_0_y = grad_u_0[:, 1]
        grad_u_1 = torch.autograd.grad(
            u_1, x_coll, grad_outputs=torch.ones_like(u_1), create_graph=True)[0]
        grad_u_1_x = grad_u_1[:, 0]
        grad_u_1_y = grad_u_1[:, 1]
        grad_v_0 = torch.autograd.grad(
            v_0, x_coll, grad_outputs=torch.ones_like(v_0), create_graph=True)[0]
        grad_v_0_x = grad_v_0[:, 0]
        grad_v_0_y = grad_v_0[:, 1]
        grad_v_1 = torch.autograd.grad(
            v_1, x_coll, grad_outputs=torch.ones_like(v_1), create_graph=True)[0]
        grad_v_1_x = grad_v_1[:, 0]
        grad_v_1_y = grad_v_1[:, 1]
        grad_p = torch.autograd.grad(
            p, x_coll, grad_outputs=torch.ones_like(p), create_graph=True)[0]
        grad_p_x = grad_p[:, 0]
        grad_p_y = grad_p[:, 1]

        grad_u_0_xx = torch.autograd.grad(grad_u_0_x, x_coll, grad_outputs=torch.ones_like(
            grad_u_0_x), create_graph=True)[0][:, 0]
        grad_u_1_xx = torch.autograd.grad(grad_u_1_x, x_coll, grad_outputs=torch.ones_like(
            grad_u_1_x), create_graph=True)[0][:, 0]
        grad_u_0_yy = torch.autograd.grad(grad_u_0_y, x_coll, grad_outputs=torch.ones_like(
            grad_u_0_y), create_graph=True)[0][:, 1]
        grad_u_1_yy = torch.autograd.grad(grad_u_1_y, x_coll, grad_outputs=torch.ones_like(
            grad_u_1_y), create_graph=True)[0][:, 1]
        grad_v_0_xx = torch.autograd.grad(grad_v_0_x, x_coll, grad_outputs=torch.ones_like(
            grad_v_0_x), create_graph=True)[0][:, 0]
        grad_v_1_xx = torch.autograd.grad(grad_v_1_x, x_coll, grad_outputs=torch.ones_like(
            grad_v_1_x), create_graph=True)[0][:, 0]
        grad_v_0_yy = torch.autograd.grad(grad_v_0_y, x_coll, grad_outputs=torch.ones_like(
            grad_v_0_y), create_graph=True)[0][:, 1]
        grad_v_1_yy = torch.autograd.grad(grad_v_1_y, x_coll, grad_outputs=torch.ones_like(
            grad_v_1_y), create_graph=True)[0][:, 1]

        res_incomp_1 = grad_u_1_x + grad_v_1_y
        res_x_1 = (u_0 * grad_u_1_x + u_1 * grad_u_0_x) + self.Re * \
            (u_1 * grad_u_1_x + v_1 * grad_u_1_y) + (v_0 * grad_u_1_y + v_1 * grad_u_0_y) + grad_p_x - \
            1. / self.Re * (grad_u_1_xx + grad_u_1_yy + 1. /
                            self.Re * (grad_u_0_xx + grad_u_0_yy))
        res_y_1 = (u_0 * grad_v_1_x + u_1 * grad_v_0_x) + self.Re * \
            (u_1 * grad_v_1_x + v_1 * grad_v_1_y) + (v_0 * grad_v_1_y + v_1 * grad_v_0_y) + grad_p_y - \
            1. / self.Re * (grad_v_1_xx + grad_v_1_yy + 1. /
                            self.Re * (grad_v_0_xx + grad_v_0_yy))

        return torch.stack([res_incomp_1, res_x_1, res_y_1])

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        x = torch.linspace(
            extrema[0, 0], extrema[0, 1], 100)
        y = torch.linspace(
            extrema[1, 0], extrema[1, 1], 100)
        grid_x, grid_y = torch.meshgrid(x, y)

        plt.figure()
        plot_var = torch.cat(
            [torch.reshape(torch.flatten(grid_x), (100*100, 1)), torch.reshape(torch.flatten(grid_y), (100*100, 1))], 1)
        out = self.get_full_solution(model, plot_var)
        u = out[:, 0].detach().numpy()
        v = out[:, 1].detach().numpy()
        p = out[:, 2].detach().numpy()

        u = u.reshape(100, 100).T
        v = v.reshape(100, 100).T
        speed = np.sqrt(u ** 2 + v ** 2)
        p = p.reshape(100, 100).T

        plt.contourf(x.detach().numpy(), y.detach(
        ).numpy(), speed)
        stream = plt.streamplot(x.detach().numpy(), y.detach(
        ).numpy(), u, v)
        plt.ylim(0, 1)

        plt.xlabel(r'$x$')
        plt.ylabel(r'$y$')
        cbar = plt.colorbar()
        cbar.set_label(r'speed')
        plt.savefig(path_to_plots + "/solution_velocity.pdf")

        plt.figure()
        plt.contourf(x.detach().numpy(), y.detach(
        ).numpy(), u)
        plt.colorbar()
        plt.savefig(path_to_plots + "/solution_velocity_u.pdf")
        plt.figure()
        plt.contourf(x.detach().numpy(), y.detach(
        ).numpy(), v)
        plt.colorbar()
        plt.savefig(path_to_plots + "/solution_velocity_v.pdf")

        plt.figure()
        cont = plt.contourf(x.detach().numpy(), y.detach(
        ).numpy(), p)
        plt.xlabel(r'$x$')
        plt.ylabel(r'$y$')
        cbar = plt.colorbar()
        cbar.set_label(r'$p$')
        plt.savefig(path_to_plots + "/solution_pressure.pdf")
