from sympy import re
from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
from PINNs.boundary_conditions import DirichletBC
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *


class Poisson(Equation):
    """
    Defines the Poisson equation.
    """

    def __init__(self):
        super(Poisson, self).__init__()

        self.type_of_points = 'sobol'
        self.output_dimension = 1
        self.space_dimensions = 2
        self.time_dimensions = 0
        self.extrema = torch.tensor([[0, 1], [0, 1]])
        self.list_of_BC = list([[self.ub0, self.ub0], [self.ub0, self.ub0]])
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

    def add_collocation_points(self, n_coll, random_seed):
        x_coll, y_coll = self.domain.add_collocation_points(
            n_coll, random_seed)
        self.f = torch.sin(
            2. * np.pi * x_coll[:, 0]) * torch.sin(2. * np.pi * x_coll[:, 1])
        return x_coll, y_coll

    def add_boundary_points(self, n_b, random_seed):
        return self.domain.add_boundary_points(n_b, random_seed)

    def apply_bc(self, model, x_b, u_b, u_pred_vars, u_train_vars):
        self.domain.apply_bc(model, x_b, u_b, u_pred_vars, u_train_vars)

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        u = model(x_coll)[:, 0].reshape(-1,)
        grad_u = torch.autograd.grad(
            u, x_coll, grad_outputs=torch.ones_like(u), create_graph=True)[0]
        grad_u_x = grad_u[:, 0]
        grad_u_y = grad_u[:, 1]
        grad_u_xx = torch.autograd.grad(
            grad_u_x, x_coll, grad_outputs=torch.ones_like(grad_u_x), create_graph=True)[0][:, 0]
        grad_u_yy = torch.autograd.grad(
            grad_u_y, x_coll, grad_outputs=torch.ones_like(grad_u_y), create_graph=True)[0][:, 1]

        residual = grad_u_xx + grad_u_yy - self.f
        return torch.unsqueeze(residual, 0)

    def exact(self, x):
        exact = - 1. / (8. * np.pi ** 2) * torch.sin(
            2. * np.pi * x[:, 0]) * torch.sin(2. * np.pi * x[:, 1])
        return exact

    def ub0(self, x):
        type_BC = [DirichletBC()]
        u = torch.full(size=(x.shape[0], 1), fill_value=0.0)
        return u, type_BC

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = self.convert(torch.rand([100000, extrema.shape[0]]), extrema)
        exact = (self.exact(x)).numpy().reshape(-1, 1)
        out = torch.squeeze(model(x)).detach().numpy().reshape(-1, 1)
        assert exact.shape[1] == out.shape[1]

        L1_error = np.mean(abs(exact - out))
        rel_L1_error = L1_error / np.mean(abs(exact))

        L2_error = np.sqrt(np.mean((exact - out) ** 2))
        rel_L2_error = L2_error / np.sqrt(np.mean(exact ** 2))

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which='both', ls=':')
            plt.scatter(exact, out)
            plt.xlabel(r'exact values')
            plt.ylabel(r'predicted values')
            plt.savefig(path_to_plots + '/score.pdf')

            x = torch.linspace(
                extrema[0, 0], extrema[0, 1], 100)
            y = torch.linspace(
                extrema[1, 0], extrema[1, 1], 100)
            grid_x, grid_y = torch.meshgrid(x, y)
            grid_x = torch.flatten(grid_x)
            grid_x = torch.reshape(grid_x, (100*100, 1))
            grid_y = torch.flatten(grid_y)
            grid_y = torch.reshape(grid_y, (100*100, 1))

            plt.figure()
            plot_var = torch.cat([grid_x, grid_y], 1)
            plt.scatter(plot_var[:, 0].detach().numpy(), plot_var[:, 1].detach(
            ).numpy(), c=((model(plot_var).detach().numpy().T - (self.exact(plot_var)).numpy()))**2)

            plt.xlabel(r'$x$')
            plt.ylabel(r'$y$')
            plt.colorbar()
            plt.savefig(path_to_plots + "/L2_error.pdf")

        return np.expand_dims(L1_error, axis=0), np.expand_dims(rel_L1_error, axis=0), np.expand_dims(L2_error, axis=0), np.expand_dims(rel_L2_error, axis=0)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        x = torch.linspace(
            extrema[0, 0], extrema[0, 1], 100)
        y = torch.linspace(
            extrema[1, 0], extrema[1, 1], 100)
        grid_x, grid_y = torch.meshgrid(x, y)
        grid_x = torch.flatten(grid_x)
        grid_x = torch.reshape(grid_x, (100*100, 1))
        grid_y = torch.flatten(grid_y)
        grid_y = torch.reshape(grid_y, (100*100, 1))

        plt.figure()
        plot_var = torch.cat([grid_x, grid_y], 1)
        plt.scatter(plot_var[:, 0].detach().numpy(), plot_var[:, 1].detach(
        ).numpy(), c=model(plot_var).detach().numpy().T)

        plt.xlabel(r'$x$')
        plt.ylabel(r'$y$')
        plt.colorbar()
        plt.savefig(path_to_plots + "/solution.pdf")
