from PINNs.equation import Equation
from PINNs.square_domain import SquareDomain
from PINNs.generate_points import generate_points
import torch
import numpy as np
import matplotlib.pyplot as plt
from PINNs.aesthetics import *


class Simple_ODE(Equation):
    """
    Defines a very simple ODE.
    """

    def __init__(self, c):
        super(Simple_ODE, self).__init__()

        self.type_of_points = "sobol"
        self.output_dimension = 1
        self.space_dimensions = 0
        self.time_dimensions = 1
        self.extrema = torch.tensor([[0, 5]])
        self.list_of_BC = list()
        self.domain = SquareDomain(self.output_dimension, self.time_dimensions,
                                   self.space_dimensions, self.list_of_BC, self.extrema, self.type_of_points)

        self.c = c
        self.u0 = torch.tensor(1)  # initial condition

    def add_collocation_points(self, n_coll, random_seed):
        return self.domain.add_collocation_points(n_coll, random_seed)

    def add_initial_points(self, n_init, random_seed):
        extrema_0 = self.extrema[:, 0]
        extrema_f = self.extrema[:, 1]
        x_time_0 = generate_points(n_init, self.time_dimensions +
                                   self.space_dimensions, random_seed, self.type_of_points, True)
        x_time_0[:, 0] = torch.full(size=(n_init,), fill_value=0.0)
        x_time_0 = x_time_0 * (extrema_f - extrema_0) + extrema_0

        return x_time_0, self.u0.repeat(n_init, 1)

    def apply_ic(self, model, x_u, u, u_pred_vars, u_train_vars):
        for j in range(self.output_dimension):
            if x_u.shape[0] != 0:
                out = model(x_u)[:, j]
                u_pred_vars.append(out)
                u_train_vars.append(u[:, j])

    def compute_res(self, model, x_coll):
        x_coll.requires_grad = True
        u = model(x_coll)
        u = u[:, 0]
        grad_u = torch.autograd.grad(
            u, x_coll, grad_outputs=torch.ones_like(u), create_graph=True)[0].reshape(-1, )

        res = self.c * grad_u + u
        return torch.unsqueeze(res, 0)

    def exact(self, x):
        exact = self.u0 * torch.exp(-x / self.c)
        return exact

    def compute_generalization_error(self, model, extrema, path_to_plots=None):
        model.eval()
        x = self.convert(torch.rand([100000, extrema.shape[0]]), extrema)
        exact = (self.exact(x)).numpy()
        exact = exact[:, 0].reshape(-1, 1)
        out = model(x).detach().numpy()
        out = out[:, 0].reshape(-1, 1)
        assert (exact.shape[1] == out.shape[1])

        L1_error = np.mean(abs(exact - out))
        rel_L1_error = L1_error / np.mean(abs(exact))

        L2_error = np.sqrt(np.mean((exact - out) ** 2))
        rel_L2_error = L2_error / np.sqrt(np.mean(exact ** 2))

        if path_to_plots is not None:
            plt.figure()
            plt.grid(True, which="both", ls=":")
            plt.scatter(exact, out, label=r'u')
            plt.xlabel(r'Exact Values')
            plt.ylabel(r'Predicted Values')
            plt.legend()
            plt.savefig(path_to_plots + "/score.pdf")

        return np.expand_dims(L1_error, axis=0), np.expand_dims(rel_L1_error, axis=0), np.expand_dims(L2_error, axis=0), np.expand_dims(rel_L2_error, axis=0)

    def plot(self, model, path_to_plots, extrema):
        model.cpu()
        model.eval()
        t = torch.reshape(torch.linspace(
            extrema[0, 0], extrema[0, 1], 100), [100, 1])

        plt.figure()
        plt.grid(True, which="both", ls=":")
        plt.plot(t, self.exact(t)[:, 0], 'b-',
                 linewidth=2, label=r'exact u', zorder=0)
        plt.scatter(t, model(t)[:, 0].detach().numpy(),
                    label=r'predicted u', marker="o", s=14, zorder=10)

        plt.xlabel(r'$t$')
        plt.legend()
        plt.savefig(path_to_plots + "/solution.pdf")
