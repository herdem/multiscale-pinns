"""
Collects data of one or multiple ensemble trainings.
Usage: python3 collect_ensemble_data.py plot_distr_bool {folder_to_analyse}*
"""

import torch
import os
import numpy as np
import sys
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pathlib
from PINNs.aesthetics import *

#! SETTINGS
selection_criterion = 'unweighted_total_loss'
evaluation_criterion = 'average_relative_L2_error'

distribution_vars = ['hidden_layers', 'n_coll',
                     'model', 'neurons', 'activation', 'w_vars', 'w_r']
distribution_labels = ['\# hidden layers',
                       '$n_{coll}$', 'model', '\# neurons', 'activation', 'weight of initial data', 'weight of residuals']
#! END SETTINGS


torch.nn.Module.dump_patches = True
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

np.random.seed(42)


def select_over_retrainings(path, criterion='unweighted_total_loss', mode='min'):
    """
    Select the retraining according to criterion and mode from path.
    Mode should be one of 'min' and 'mean'.
    """
    retrain_paths = [d for d in os.listdir(
        path) if os.path.isdir(path + '/' + d) and not os.path.islink(path + '/' + d)]

    data = list()
    for retrain in retrain_paths:
        retrain_id = int(retrain.split('_')[-1])
        retrain_path = path + '/' + retrain

        if os.path.isfile(retrain_path + '/' + 'data.csv'):
            retrain_data = pd.read_csv(
                retrain_path + '/' + 'data.csv', header=0)
            retrain_data['retrain_id'] = retrain_id

            data.append(retrain_data)
        else:
            print('No data file found in ' + retrain_path)

    data = pd.concat(data, ignore_index=True)
    data = data.sort_values(criterion)

    if mode == 'min':
        return data.iloc[:1]
    elif mode == 'mean':
        retrain_id = data['retrain_id'].iloc[0]
        data = data.mean()
        data['retrain_id'] = retrain_id
        return data
    else:
        raise ValueError()


if len(sys.argv) < 3:
    raise ValueError()

if str(sys.argv[1]) == 'true':
    plot_distr = True
else:
    plot_distr = False

ensemble_folders = [str(sys.argv[i]) for i in range(2, len(sys.argv))]

for ensemble_folder in ensemble_folders:
    data = list()
    setup_paths = [d for d in os.listdir(
        ensemble_folder) if os.path.isdir(ensemble_folder + '/' + d) and not os.path.islink(ensemble_folder + '/' + d)]

    for setup_path in setup_paths:
        absolute_setup_path = ensemble_folder + '/' + setup_path

        # find best retraining in this setup
        selected_retrain = select_over_retrainings(
            absolute_setup_path, criterion=selection_criterion)
        setup_config = pd.read_json(absolute_setup_path + '/config.json')
        if setup_config['batch_size'].values[0] == 'full':
            setup_config['batch_size'] = setup_config['n_coll'] + \
                setup_config['n_u'] + setup_config['n_int']
        setup_id = int(setup_path.split('_')[-1])
        setup_config['setup_id'] = setup_id
        setup_retrain = pd.merge(setup_config.reset_index(
            drop=True), selected_retrain.reset_index(drop=True), left_index=True, right_index=True)
        data.append(setup_retrain)

    # find best overall setup for this ensemble
    data = pd.concat(data, ignore_index=True)
    data = data.sort_values(selection_criterion)
    best = data.iloc[:1]
    best_setup = best['setup_id'].values[0]
    best_retrain = best['retrain_id'].values[0]

    # create symlink to best setup and retrain
    path = ensemble_folder + '/setup_' + \
        str(best_setup) + '/retrain_' + str(best_retrain) + '/'
    if not os.path.islink(ensemble_folder + '/best_setup'):
        os.symlink(str(pathlib.Path().resolve()) + '/' +
                   path, ensemble_folder + '/best_setup')

    if plot_distr:
        total = list()
        for var in distribution_vars:
            values = data[var].values
            values = list(set(values))
            values.sort()

            df_values = list()
            for value in values:
                indices = data.index[data[var] == value]
                df_values.append(data.loc[indices])
            total.append(df_values)

        for i in range(len(total)):
            var = distribution_vars[i]
            label = distribution_labels[i]
            sensitivities = total[i]

            figure = plt.figure()
            axes = plt.gca()
            plt.grid(True, which='both', ls=':')

            i = 0
            for sensitivity in sensitivities:
                value = sensitivity[var].values[0]
                label_ = label + " = " + str(value).replace('_', '-')

                sns.distplot(sensitivity[evaluation_criterion], label=label_, kde=True,
                             hist=True, norm_hist=False, kde_kws={'linewidth': 2})
                kdeline = axes.lines[i]
                mean = sensitivity[evaluation_criterion].mean()
                xs = kdeline.get_xdata()
                ys = kdeline.get_ydata()
                height = np.interp(mean, xs, ys)
                axes.vlines(mean, 0, height, color=axes.get_lines()
                            [-1].get_c(), ls=':', lw=2)
                axes.fill_between(xs, 0, ys, facecolor=axes.get_lines()
                                  [-1].get_c(), alpha=0.2)
                i += 1

            plt.xlabel(r'$\varepsilon_G$')
            plt.legend(loc=1)
            plt.savefig(ensemble_folder + '/sensitivity_' +
                        var + ".pdf")
