"""
Compare the small scale of different ensembles.
"""

import torch
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from PINNs.aesthetics import *
from torchdiffeq import odeint
from PINNs.square_domain import SquareDomain

torch.nn.Module.dump_patches = True
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

ensembles = ['data/ensembles_c5e10/c5e10_full',
             'data/ensembles_c5e10/c5e10_separated', 'data/ensembles_c5e10/c5e10_small_scale']

c = 1e-5  # control the scales
u0 = torch.tensor(0)  # initial condition
du0dt = torch.tensor(0)  # initial condition
extrema = torch.tensor([[0, 10]])

x = torch.reshape(torch.linspace(
    extrema[0, 0], extrema[0, 1], 100000), [100000, 1])

domain = SquareDomain(1, 1, 0, list(), extrema, 'sobol')
collocation_points, _ = domain.add_collocation_points(2048, 42)


def integrated_sol(input):
    def f(t, y):
        x = torch.Tensor([y[0]])
        dx_dt = torch.Tensor([y[1]])
        d2x_dt2 = torch.Tensor(
            [-(x + c * (x + torch.cos(t)) ** 3)])
        return torch.cat([dx_dt, d2x_dt2])

    return odeint(f, torch.stack([u0, du0dt]).float(), torch.flatten(input))[:, 0]


int_sol = integrated_sol(x).numpy().reshape(-1, 1)

#####################################
# Naive version
#####################################

setup_paths = [d for d in os.listdir(
    ensembles[0]) if os.path.isdir(ensembles[0] + '/' + d)]

data_naive = list()
for setup_path in setup_paths:
    absolute_setup_path = ensembles[0] + '/' + setup_path

    retrain_paths = [d for d in os.listdir(
        absolute_setup_path) if os.path.isdir(absolute_setup_path + '/' + d)]

    data = list()
    for retrain in retrain_paths:
        retrain_id = int(retrain.split('_')[-1])
        retrain_path = absolute_setup_path + '/' + retrain

        if os.path.isfile(retrain_path + '/' + 'data.csv'):
            retrain_data = pd.read_csv(
                retrain_path + '/' + 'data.csv', header=0)
            retrain_data['retrain_id'] = retrain_id
        else:
            print('No data file found in ' + retrain_path)
            continue

        model = torch.load(retrain_path + '/model_vanilla.pt')
        model.eval()
        collocation_points.requires_grad = True
        small_scale = model(collocation_points)[
            :, 0] - torch.cos(torch.squeeze(collocation_points))
        grad_u = torch.autograd.grad(
            small_scale, collocation_points, grad_outputs=torch.ones_like(small_scale), create_graph=True)[0][:, 0]
        grad_u_tt = torch.autograd.grad(grad_u, collocation_points, grad_outputs=torch.ones_like(
            small_scale), create_graph=True)[0][:, 0].reshape(-1,)
        residual_loss_small_scale = grad_u_tt + small_scale + \
            c * (small_scale + torch.cos(torch.squeeze(collocation_points))) ** 3
        residual_loss_small_scale = torch.mean(
            abs(residual_loss_small_scale) ** 2, -1)
        small_scale = (
            model(x)[:, 0] - torch.cos(torch.squeeze(x))).detach().numpy().reshape(-1, 1)
        rel_L2_error_small_scale = np.sqrt(
            np.mean((small_scale - int_sol) ** 2)) / np.sqrt(np.mean(int_sol ** 2))

        retrain_data['relative_L2_error_2'] = rel_L2_error_small_scale
        retrain_data['small_scale_vars_loss'] = residual_loss_small_scale.detach().item() + \
            retrain_data['vars_loss']
        data.append(retrain_data)

    data = pd.concat(data, ignore_index=True)
    data = data.sort_values('small_scale_vars_loss')

    selected_retrain = data.iloc[:1]

    setup_config = pd.read_json(absolute_setup_path + '/config.json')
    if setup_config['batch_size'].values[0] == 'full':
        setup_config['batch_size'] = setup_config['n_coll'] + \
            setup_config['n_u'] + setup_config['n_int']
    setup_id = int(setup_path.split('_')[-1])
    setup_config['setup_id'] = setup_id
    setup_retrain = pd.merge(setup_config.reset_index(
        drop=True), selected_retrain.reset_index(drop=True), left_index=True, right_index=True)
    data_naive.append(setup_retrain)

data_naive = pd.concat(data_naive, ignore_index=True)
print('Analyzed naive')

#####################################
# Scale separation version
#####################################

setup_paths = [d for d in os.listdir(
    ensembles[1]) if os.path.isdir(ensembles[1] + '/' + d)]

data_scale_sep = list()
for setup_path in setup_paths:
    absolute_setup_path = ensembles[1] + '/' + setup_path

    retrain_paths = [d for d in os.listdir(
        absolute_setup_path) if os.path.isdir(absolute_setup_path + '/' + d)]

    data = list()
    for retrain in retrain_paths:
        retrain_id = int(retrain.split('_')[-1])
        retrain_path = absolute_setup_path + '/' + retrain

        if os.path.isfile(retrain_path + '/' + 'data.csv'):
            retrain_data = pd.read_csv(
                retrain_path + '/' + 'data.csv', header=0)
            retrain_data['retrain_id'] = retrain_id
        else:
            print('No data file found in ' + retrain_path)
            continue

        retrain_data['small_scale_vars_loss'] = retrain_data['residual_loss_2'] + \
            retrain_data['vars_loss']
        data.append(retrain_data)

    data = pd.concat(data, ignore_index=True)
    data = data.sort_values('small_scale_vars_loss')

    selected_retrain = data.iloc[:1]

    setup_config = pd.read_json(absolute_setup_path + '/config.json')
    if setup_config['batch_size'].values[0] == 'full':
        setup_config['batch_size'] = setup_config['n_coll'] + \
            setup_config['n_u'] + setup_config['n_int']
    setup_id = int(setup_path.split('_')[-1])
    setup_config['setup_id'] = setup_id
    setup_retrain = pd.merge(setup_config.reset_index(
        drop=True), selected_retrain.reset_index(drop=True), left_index=True, right_index=True)
    data_scale_sep.append(setup_retrain)

data_scale_sep = pd.concat(data_scale_sep, ignore_index=True)
print('Analyzed scale sep')

#####################################
# Small scale learned version
#####################################

setup_paths = [d for d in os.listdir(
    ensembles[2]) if os.path.isdir(ensembles[2] + '/' + d)]

data_small = list()
for setup_path in setup_paths:
    absolute_setup_path = ensembles[2] + '/' + setup_path

    retrain_paths = [d for d in os.listdir(
        absolute_setup_path) if os.path.isdir(absolute_setup_path + '/' + d)]

    data = list()
    for retrain in retrain_paths:
        retrain_id = int(retrain.split('_')[-1])
        retrain_path = absolute_setup_path + '/' + retrain

        if os.path.isfile(retrain_path + '/' + 'data.csv'):
            retrain_data = pd.read_csv(
                retrain_path + '/' + 'data.csv', header=0)
            retrain_data['retrain_id'] = retrain_id
        else:
            print('No data file found in ' + retrain_path)
            continue

        retrain_data['small_scale_vars_loss'] = retrain_data['residual_loss_1'] + \
            retrain_data['vars_loss']
        data.append(retrain_data)

    data = pd.concat(data, ignore_index=True)
    data = data.sort_values('small_scale_vars_loss')

    selected_retrain = data.iloc[:1]

    setup_config = pd.read_json(absolute_setup_path + '/config.json')
    if setup_config['batch_size'].values[0] == 'full':
        setup_config['batch_size'] = setup_config['n_coll'] + \
            setup_config['n_u'] + setup_config['n_int']
    setup_id = int(setup_path.split('_')[-1])
    setup_config['setup_id'] = setup_id
    setup_retrain = pd.merge(setup_config.reset_index(
        drop=True), selected_retrain.reset_index(drop=True), left_index=True, right_index=True)
    data_small.append(setup_retrain)

data_small = pd.concat(data_small, ignore_index=True)
print('Analyzed small scale')

#####################################
# Plotting
#####################################

# filter out all points that have relative L2 error 100% or bigger
data_ = [data_naive[data_naive['relative_L2_error_2'] < 1], data_scale_sep[data_scale_sep['relative_L2_error_2']
                                                                           < 1], data_small[data_small['relative_L2_error_1'] < 1]]
data = [data_naive[data_naive['relative_L2_error_2'] < 1]['relative_L2_error_2'], data_scale_sep[data_scale_sep['relative_L2_error_2']
                                                                                                 < 1]['relative_L2_error_2'], data_small[data_small['relative_L2_error_1'] < 1]['relative_L2_error_1']]
labels = ['naive', 'scales learned together', 'small scale learned']

# Error plots
plt.figure()
plt.grid(True, which='both', ls=':')
for i in range(len(data)):
    plt.scatter(data_[i]['small_scale_vars_loss'],
                data[i], label=labels[i])
plt.xlabel(r'$\varepsilon_T$')
plt.ylabel(r'$\varepsilon_G$')
# plt.yscale('log')
plt.xscale('log')
plt.legend()
plt.savefig('et_vs_eg_compared_small_scale.pdf')
print('Plotted errors')

# Distribution plots
figure = plt.figure()
axes = plt.gca()
plt.grid(True, which='both', ls=':')

for i in range(len(data)):
    sns.distplot(data[i], label=labels[i], kde=True,
                 hist=True, norm_hist=False, kde_kws={'linewidth': 2})
    kdeline = axes.lines[i]
    mean = data[i].mean()
    xs = kdeline.get_xdata()
    ys = kdeline.get_ydata()
    height = np.interp(mean, xs, ys)
    axes.vlines(mean, 0, height, color=axes.get_lines()
                [-1].get_c(), ls=':', lw=2)
    axes.fill_between(xs, 0, ys, facecolor=axes.get_lines()
                      [-1].get_c(), alpha=0.2)

plt.xlabel(r'$\varepsilon_G$')
plt.legend(loc=1)
plt.savefig('generalization_distr_small_scales.pdf')
print('Plotted distros')
