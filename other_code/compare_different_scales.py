"""
Compare different scales using this script. 
It loads two different models (the best from their corresponding ensembles), 
removes the big scale from the first model and then compares only the small scales.
Expects symlinks to the best setups, i.e. collect_ensemble_data.py should be run prior to usage.
Usage: python3 compare_different_scales.py all_scales_ensemble small_scale_ensemble
"""

import torch
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
from torchdiffeq import odeint
from PINNs.aesthetics import *

torch.manual_seed(42)

#! SETTINGS
extrema = torch.tensor([[0, 10]])
c = 0.00001
eval_points = torch.reshape(torch.linspace(
    extrema[0, 0], extrema[0, 1], 10000), [10000, 1])
big_scale = torch.cos(eval_points)


def integrated_small_scale(input):
    def f(t, y):
        x = torch.Tensor([y[0]])
        dx_dt = torch.Tensor([y[1]])
        d2x_dt2 = torch.Tensor([-x - c * (x + torch.cos(t)) ** 3])
        return torch.cat([dx_dt, d2x_dt2])

    return odeint(f, torch.stack([torch.tensor(0), torch.tensor(0)]).float(), torch.flatten(input))[:, 0]
#! END SETTINGS


if len(sys.argv) != 3:
    raise ValueError()

ensemble_all = str(sys.argv[1])
ensemble_small = str(sys.argv[2])

print('Loading models...')
# load the solutions
model_all = torch.load(ensemble_all + '/best_setup/model_vanilla.pt')
model_all.eval()
model_small = torch.load(ensemble_small + '/best_setup/model_vanilla.pt')
model_small.eval()

print('Evaluating models...')
small_scale_naive = model_all(eval_points) - big_scale
small_scale_trained = model_small(eval_points)

integrated = integrated_small_scale(eval_points)
integrated = torch.unsqueeze(integrated, 1)

L1_naive = abs(small_scale_naive - integrated).detach().numpy()
L1_small = abs(small_scale_trained - integrated).detach().numpy()

dir = 'comparison_small_scales'
os.mkdir(dir)

rel_err_naive = np.sqrt(np.mean(L1_naive ** 2)) / \
    np.sqrt(np.mean(integrated.detach().numpy() ** 2))
rel_err_small = np.sqrt(np.mean(L1_small ** 2)) / \
    np.sqrt(np.mean(integrated.detach().numpy() ** 2))

print(f'Relative error naive: {rel_err_naive}')
print(f'Relative error small scale: {rel_err_small}')

with open(dir + '/errors.csv', 'w') as f:
    f.write('relative_error_naive,relative_error_small_scale\n')
    f.write(str(rel_err_naive) + ',' + str(rel_err_small))

plt.figure()
plt.grid(True, which='both', ls=':')
plt.plot(eval_points[::10].detach().numpy(),
         L1_naive[::10], linewidth=2, zorder=0, label=r'$|(u_{duffing} - cos(t)) - u_{integrated}|$')
plt.plot(eval_points[::10].detach().numpy(),
         L1_small[::10], linewidth=2, zorder=0, label=r'$|u_{small} - u_{integrated}|$')
plt.xlabel(r'$t$')
plt.legend()
plt.savefig(dir + '/error.pdf')

plt.figure()
plt.grid(True, which='both', ls=':')
plt.scatter(eval_points[::10].detach().numpy(
), small_scale_naive[::10].detach().numpy(), s=14, zorder=10, label=r'naive')
plt.scatter(eval_points[::10].detach().numpy(), small_scale_trained[::10].detach(
).numpy(), s=14, zorder=10, label=r'small scale')
plt.plot(eval_points[::10].detach().numpy(), integrated[::10].detach(
).numpy(), linewidth=2, label=r'integrated', zorder=0)
plt.xlabel(r'$t$')
plt.legend()
plt.savefig(dir + '/solutions.pdf')
