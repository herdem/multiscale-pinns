import torch
from torchdiffeq import odeint
import matplotlib.pyplot as plt

c = 0.1
extrema = torch.tensor([[0, 10]])
u0 = torch.tensor(1)
u0_eps = torch.tensor(0)
du0dt = torch.tensor(0)
du0dt_eps = torch.tensor(0)


def direct_y(input):
    def f(t, y):
        x = torch.Tensor([y[0]])
        dx_dt = torch.Tensor([y[1]])
        d2x_dt2 = torch.Tensor([-(x + c * x ** 3)])
        return torch.cat([dx_dt, d2x_dt2])

    return odeint(f, torch.stack([u0, du0dt]).float(), torch.flatten(input))[:, 0]


def y_eps(input):
    def f(t, y):
        x = torch.Tensor([y[0]])
        dx_dt = torch.Tensor([y[1]])
        d2x_dt2 = torch.Tensor([-(x + c * (x + torch.cos(t)) ** 3)])
        return torch.cat([dx_dt, d2x_dt2])

    return odeint(f, torch.stack([u0_eps, du0dt_eps]).float(), torch.flatten(input))[:, 0]


t = torch.reshape(torch.linspace(
    extrema[0, 0], extrema[0, 1], 1000), [1000, 1])

direct_sol = direct_y(t)
sol_eps = y_eps(t)
sol_0 = torch.squeeze(torch.cos(t))
y_splitted = sol_eps + sol_0

fig, axs = plt.subplots(2, 2, sharex=True)

axs[0, 0].plot(t, sol_eps, 'b-',
               label=r'$y_\epsilon$', zorder=0)
axs[0, 1].plot(t, abs(direct_sol - y_splitted)**2, 'c+',
               label=r'pointwise $L2$ error', zorder=0)
axs[1, 0].plot(t, y_splitted, 'm-',
               label=r'$y_{scale-split}$', zorder=0)
axs[1, 0].set(xlabel=r'$t$')
axs[1, 1].plot(t, direct_sol, 'r-',
               label=r'$y_{direct}$', zorder=0)
axs[1, 1].set(xlabel=r'$t$')

for ax in axs.flat:
    ax.grid(True, which="both", ls=":")

fig.legend(loc='right', bbox_to_anchor=(0, 0.5))

plt.show()
# fig.savefig("y_" + str(c) + ".pdf", dpi=500,
#            bbox_inches='tight')
