import torch
from torchdiffeq import odeint

extrema = torch.tensor([[0, 10]])
c = 1e-5  # control the scales
u0 = torch.tensor([1, 0])  # initial condition
du0dt = torch.tensor([0, 0])  # initial condition


def integrated_sol(input):
    def f(t, y):
        y_big = torch.Tensor([y[0]])
        z_big = torch.Tensor([y[1]])
        y_small = torch.Tensor([y[2]])
        z_small = torch.Tensor([y[3]])
        return torch.cat([z_big, -y_big, z_small, -y_small - c * (y_big + y_small) ** 3])

    return odeint(f, torch.stack([u0[0], du0dt[0], u0[1], du0dt[1]]).float(), torch.flatten(input))[:, ::2]


t = torch.reshape(torch.linspace(
    extrema[0, 0], extrema[0, 1], 100), [100, 1])

y = integrated_sol(t)
print(y)
