import pandas as pd

datafile = 'plots/conv_diff/best_overall_conv_diff_300.csv'
data = pd.read_csv(datafile, index_col=0)
print(data)
idx_naive = [1, 0]
idx_small = []
idx_scale_sep = [4, 6]


def print_data(label, col_name):
    print(label, end='')
    for i in idx_naive:
        print(' & ' + str(data.iloc[i, :][col_name]), end='')
    for i in idx_small:
        print(' & ' + str(data.iloc[i, :][col_name]), end='')
    for i in idx_scale_sep:
        print(' & ' + str(data.iloc[i, :][col_name]), end='')
    print(r'\\')


print_data(r'architecture', 'model')
print_data(r'$k$', 'hidden_layers')
print_data(r'$s$', 'neurons')
print_data(r'$\sigma$', 'activation')
print(r'$w_r$ & DO_BY_HAND\\')
print_data(r'$w_u$', 'w_vars')
print_data(r'$w_{\text{reg}}$', 'w_reg')


def print_errors(label, col_names, gen=True):
    if gen:
        mul = 100.
    else:
        mul = 1.
    print(label, end='')
    j = 0
    for i in idx_naive:
        print(
            ' & ' + '{:e}'.format(data.iloc[i, :][col_names[j]] * mul), end='')
        j += 1
    for i in idx_small:
        print(
            ' & ' + '{:e}'.format(data.iloc[i, :][col_names[j]] * mul), end='')
        j += 1
    for i in idx_scale_sep:
        print(
            ' & ' + '{:e}'.format(data.iloc[i, :][col_names[j]] * mul), end='')
        j += 1
    print(r'\\')


print_errors(r'$\varepsilon_T$', ['unweighted_total_loss'] * 2 + [
             'unweighted_total_loss'] * 0 + ['unweighted_total_loss'] * 2, False)
print_errors(r'$\varepsilon_G^{L^1}$', [
             'average_relative_L1_error'] * 2 + ['relative_L1_error_1'] * 0 + ['relative_L1_error_full'] * 2)
print_errors(r'$\varepsilon_G^{L^2}$', [
             'average_relative_L2_error'] * 2 + ['relative_L2_error_1'] * 0 + ['relative_L2_error_full'] * 2)
