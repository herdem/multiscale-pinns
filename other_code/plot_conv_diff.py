import sys
import pathlib
sys.path.insert(1, str(pathlib.Path(__file__).parent.resolve()) + '/..')

from PINNs.aesthetics import *
import numpy as np


def exact(x, c):
    y1 = np.log(np.exp(c * (x - 1)) - np.exp(-c))
    y2 = np.log(1. - np.exp(-c))
    return x - np.exp(y1 - y2)


x = np.linspace(0, 1, 1000)

plt.figure()
plt.grid(True, which="both", ls=":")
plt.plot(x, exact(x, 5), label=r'$u=5$', linewidth=2)
plt.plot(x, exact(x, 20), label=r'$u=20$', linewidth=2)
plt.plot(x, exact(x, 200), label=r'$u=200$', linewidth=2)
plt.xlabel(r'$x$')
plt.ylabel(r'$T(x)$')
plt.legend()
plt.savefig('conv_diff_exact_sol.pdf')
