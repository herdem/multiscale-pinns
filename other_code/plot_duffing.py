import sys
import pathlib
sys.path.insert(1, str(pathlib.Path(__file__).parent.resolve()) + '/..')

from PINNs.aesthetics import *
import numpy as np
import torch
from torchdiffeq import odeint
import torch


def exact(input, c):
    def f(t, y):
        x = torch.Tensor([y[0]])
        dx_dt = torch.Tensor([y[1]])
        d2x_dt2 = torch.Tensor([-(x + c * x ** 3)])
        return torch.cat([dx_dt, d2x_dt2])

    return odeint(f, torch.stack([torch.tensor(1.), torch.tensor(0.)]).float(), torch.flatten(input))[:, 0]


x = torch.linspace(0, 10, 1000)

plt.figure()
plt.grid(True, which="both", ls=":")
plt.plot(x.detach().numpy(), exact(x, 1).detach().numpy(),
         label=r'$\epsilon=1$', linewidth=2)
plt.plot(x.detach().numpy(), exact(x, 0.1).detach().numpy(),
         label=r'$\epsilon=0.1$', linewidth=2)
plt.plot(x.detach().numpy(), exact(x, 0.01).detach().numpy(),
         label=r'$\epsilon=0.01$', linewidth=2)
plt.plot(x.detach().numpy(), np.cos(x.detach().numpy()),
         label=r'$\cos(t)$', linewidth=2, color='grey', linestyle='dotted')
plt.xlabel(r'$t$')
plt.ylabel(r'$u(t)$')
plt.legend(loc=3)
plt.savefig('duffing_exact_sol.pdf')
