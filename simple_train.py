"""
Trains a PINN using paramater given in config.json.
Usage: python3 simple_train.py path_to_config folder_path retrain_seed compute_generalization_error_bool plot_bool log_bool
"""

import torch
import sys
import os
from PINNs.dataset import Dataset
import PINNs.PINN as PINN
import PINNs.PINN_resnet as PINN_resnet
import PINNs.PINN_densenet as PINN_densenet
import PINNs.mPINN as mPINN
from PINNs.dirichlet_model_integration import Dirichlet_Wrapper
from PINNs.fit import fit
import time
import torch.optim as optim
from PINNs.aesthetics import *
import numpy as np
import PINNs.io.input as input
import PINNs.io.output as output
from torch.utils.tensorboard import SummaryWriter


# exchange this line to switch equations
#! EQUATION SPECIFIC
from equations.forward_problems.navier_stokes_scale_separation import Navier_Stokes_scale_separation as equation
args = {
    "Re": 100.
}
#! END EQUATION SPECIFIC
with_dirichlet_wrapper = False
# torch.tensor([[1., 1. / args['c']]])
rescale = torch.tensor([[1., 1. / args['Re'], 1., 1. / args['Re'], 1.]])
mPINN_bool = False
log_frequency = 1

torch.manual_seed(42)
sampling_seed = 128
os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

eq = equation(**args)


def G(x, a, b):
    u = eq.boundary_fn_u(torch.unsqueeze(x[:, 1], 1))
    vp = torch.full(size=(x.shape[0], 4), fill_value=0.)
    return torch.cat([u, vp], dim=-1)

# def G(x, a, b):
#    return torch.tensor([[1., -1. / args['c']]]) * (x - a)


if len(sys.argv) != 7:
    raise ValueError()

path_to_config = str(sys.argv[1])
folder_path = str(sys.argv[2])
retrain = int(sys.argv[3])
if str(sys.argv[4]) == 'true':
    compute_generalization_error = True
else:
    compute_generalization_error = False
if str(sys.argv[5]) == 'true':
    plot = True
else:
    plot = False

if os.path.isdir(folder_path):
    print('Directory already exists. Abort.')
    exit()

os.mkdir(folder_path)

if str(sys.argv[6]) == 'true':
    log_path = folder_path + '/log'
    if not os.path.isdir(log_path):
        os.mkdir(log_path)
    logger = SummaryWriter(log_dir=log_path)
    config = input.read_json(path_to_config)
else:
    logger = None

n_coll, n_u, n_int, model, num_hidden_layers, num_neurons, activation_f, optimizer, num_epochs, max_iter, batch_size, shuffle, loss_weights, regularization_order = input.init(
    path_to_config, folder_path)
input.save_args(args, folder_path)

# infer dimensions
if eq.extrema is not None:
    extrema = eq.extrema
else:
    print('Using free shape.')
    extrema = None

space_dimensions = eq.space_dimensions
time_dimension = eq.time_dimensions

try:
    parameter_values = eq.parameter_values
    parameter_dimensions = parameter_values.shape[0]
except AttributeError:
    parameter_values = None
    parameter_dimensions = 0

input_dimensions = parameter_dimensions + time_dimension + space_dimensions
output_dimension = eq.output_dimension

# infer number of points
n = n_u + n_coll + n_int

if space_dimensions > 0:
    n_b = n_u // (4 * space_dimensions)
else:
    n_b = 0
if time_dimension == 1:
    n_i = n_u - 2 * space_dimensions * n_b
elif time_dimension == 0:
    n_b = n_u // (2 * space_dimensions)
    n_i = 0
else:
    raise ValueError()

if batch_size == 'full':
    batch_size = n

# dataset
dataset = Dataset(eq, n_coll, n_b, n_i, n_int, batches=batch_size,
                  random_seed=sampling_seed, shuffle=shuffle)

# model
if model == 'vanilla':
    model = PINN.PINN
elif model == 'resnet':
    model = PINN_resnet.PINN_resnet
elif model == 'densenet':
    model = PINN_densenet.PINN_densenet
else:
    raise ValueError()

if mPINN_bool:
    model = mPINN.mPINN(model, input_dimensions, output_dimension,
                        num_hidden_layers, num_neurons, activation_f)
else:
    model = model(input_dimensions, output_dimension,
                  num_hidden_layers, num_neurons, activation_f)

if with_dirichlet_wrapper:
    model = Dirichlet_Wrapper(model, eq, rescale, G)

# initialize weights
torch.manual_seed(retrain)
PINN.init_xavier(model)

if num_epochs != 1:
    max_iter = 1

# optimizer
if optimizer == 'ADAM':
    optimizer = optim.Adam(model.parameters(), lr=0.0005)
elif optimizer == 'LBFGS':
    if num_epochs != 1 and max_iter == 1 and (batch_size == "full" or batch_size == n):
        print(bcolors.WARNING + "WARNING: you set max_iter=1 and epochs=" + str(num_epochs) +
              " with a LBFGS optimizer.\n This will work but it is not efficient in full batch mode. Set max_iter = " + str(num_epochs) + " and epochs=1. instead" + bcolors.ENDC)
    optimizer = optim.LBFGS(model.parameters(), lr=0.8, max_iter=max_iter, max_eval=50000, history_size=100,
                            line_search_fn="strong_wolfe",
                            tolerance_change=1.0 * np.finfo(float).eps)
else:
    raise ValueError()

# training
model.to(eq.device)
model.train()

start_time = time.time()
losses = fit(eq, model, optimizer, num_epochs, dataset,
             loss_weights, regularization_order=regularization_order, logger=logger, log_frequency=log_frequency)
training_time = time.time() - start_time

if plot:
    path_to_plots = folder_path + '/plots'
    os.mkdir(path_to_plots)
else:
    path_to_plots = None

# write training data to csv, save model
header, row = output.prepare_training_data(training_time, losses)
header.append('retrain_seed')
row.append(retrain)
output.save_model(model, folder_path)

if compute_generalization_error:
    h, r = output.prepare_generalization_data(
        eq, model, extrema, path_to_plots)
    header += h
    row += r

output.save_data(header, row, folder_path)

if plot:
    eq.plot(model, path_to_plots, extrema)

if logger != None:
    metric_dict = dict()
    config['w_r'] = torch.Tensor(config['w_r'])
    for (k, v) in zip(header, row):
        metric_dict[k] = v
    logger.add_hparams(config, metric_dict)
    logger.flush()
    logger.close()
